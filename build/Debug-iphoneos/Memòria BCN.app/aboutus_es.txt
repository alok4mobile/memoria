La investigación y los textos de esta aplicación han sido realizados por: 

Associació Conèixer Història

Director: Jordi Guixé i Corominas
Coordinador: Oriol López i Badell

La Asociación Conèixer Història es una entidad sin ánimo de lucro que tiene por objetivo investigar y difundir el pasado para entender mejor el presente y alcanzar un futuro más justo y democrático. Con este proyecto, propone una serie de rutas por espacios de historia y de memoria de la Barcelona del siglo XX. Una ciudad en la que se proclamó la República Catalana, que fue bombardeada sistemáticamente durante la Guerra Civil española y que vivió en sus calles la lucha por recuperar la democracia, debe tener conciencia de los espacios dónde estos hechos sucedieron. La aplicación para móviles, impulsada paralelamente por el Ayuntamiento de Barcelona, permite recorrer a pie las rutas aquí propuestas con toda la información a mano.

Versión 1.01