//
//  HM_Data.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HM_EventData : NSObject

@property (nonatomic, retain) NSString *route;
@property (nonatomic) int orderRoute;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSMutableArray *events;
@property (nonatomic, retain) NSString *textC;
@property (nonatomic, retain) NSString *textL;
@property (nonatomic, retain) NSString *photoA;
@property (nonatomic, retain) NSString *photoB;
@property (nonatomic, retain) NSString *latitude;
@property (nonatomic, retain) NSString *longitude;

- (id) initWithDictionary:(NSDictionary*)dictionary;


@end
