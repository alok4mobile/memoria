//
//  HM_DataModel.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HM_EventData.h"

@interface HM_DataModel : NSObject {
	NSMutableArray *_eventsArray;
	NSMutableDictionary *_events;
	NSMutableDictionary *_eventsByEventName;
	NSMutableDictionary *_eventsByRouteName;
	NSMutableArray *_favoriteEvents;
}

@property (nonatomic,strong,readonly) NSArray *eventsArray;
@property (nonatomic,strong,readonly) NSDictionary *events;
@property (nonatomic,strong,readonly) NSDictionary *eventsByEventName;
@property (nonatomic,strong,readonly) NSDictionary *eventsByRouteName;
@property (nonatomic,strong,readonly) NSArray *favoriteEvents;

-(id)init;
-(void)addFavoriteEvent:(HM_EventData *)eventData;
-(void)removeFavoriteEvent:(NSString *)eventData;
-(BOOL)isPageFavorite:(HM_EventData *)eventData;

- (NSArray*) routes;

- (NSArray *) eventWithRoute:(NSString *)route;
- (NSArray *) RouteWithEvent:(NSString *)event;
- (NSArray *) namesWithEventandRoute:(NSString*)event:(NSString*)route;
- (NSArray *) searchResults:(NSString*)event:(NSString*)route:(NSString*)name;

@end
