//
//  HM_Data.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_EventData.h"

@implementation HM_EventData

@synthesize route = _route,orderRoute = _orderRoute,name = _name,address = _address,events = _events, textC = _textC, textL = _textL, photoA = _photoA, photoB = _photoB, latitude = _latitude, longitude = _longitude;

- (id)initWithDictionary:(NSDictionary *)value {
	self = [super init];
	if (self) {
		
		NSString* eventsWithNoSpaces = [[value objectForKey:@"Event"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; // get rid of white spaces before and after name
		
		NSArray *eventStringWithNoSpaces = [NSArray new];
		NSMutableArray *eventSet = [NSMutableArray new];
		
		eventStringWithNoSpaces = [eventsWithNoSpaces componentsSeparatedByString:@"/"];
		
		for (NSString *tempString in eventStringWithNoSpaces) {
			[eventSet addObject:[tempString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
			//NSLog(@"added: <%@>",[tempString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
		}
		self.events = [NSArray arrayWithArray:eventSet];
		
		NSString* routeWithNoSpaces = [[value objectForKey:@"Route"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; // get rid of white spaces before and after name
		self.route = [routeWithNoSpaces stringByReplacingOccurrencesOfString:@"," withString:@"\n"];	
		self.name = [value objectForKey:@"Name"];
		self.address = [value objectForKey:@"Address"];
		self.textC = [value objectForKey:@"Text C"];
		self.textL = [value objectForKey:@"Text L"];
		self.photoA = [value objectForKey:@"Photo A"];
		self.photoB = [value objectForKey:@"Photo B"];
		self.latitude = [value objectForKey:@"Latitud"];
		self.longitude = [value objectForKey:@"Longitud"];
	}
	return self;
}


@end
