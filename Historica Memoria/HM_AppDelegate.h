//
//  HM_AppDelegate.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HM_DataModel.h"
#import "Argol.h"

@interface HM_AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate,UIAppearanceContainer>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) HM_DataModel *data;

@property (strong, nonatomic) ArgolDevice *gps;

@property (strong, nonatomic) NSMutableArray *targetPOI;

+ (HM_AppDelegate*) sharedAppDelegate;
-(void)clearData;
//-(void)hideTitle;
//-(void)showTitle;
-(void)startGPS;
-(void)loadTargets;
-(void)stopGPS;


@end
