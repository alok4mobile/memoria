//
//  HM_FullPagePOIViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 11/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HM_FullPagePOIViewController : UIViewController<UIScrollViewDelegate> {
	
	UIImage *_imageA;
	UIImage *_imageB;
	UIPageControl *_pageControl;
	UIImageView *arImageA;
	UIImageView *arImageB;
	UIScrollView *surfaceScroll;
}

- (id)initWithImageA:(NSString *)imageA ImageB:(NSString *)imageB;
@end
