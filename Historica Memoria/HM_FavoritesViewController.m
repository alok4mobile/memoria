//
//  HM_FavoritesViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_FavoritesViewController.h"
#import "HM_AppDelegate.h"

@interface HM_FavoritesViewController () {
	NSArray *_favRoutes;
	UITableView *_tableView;
}

@end

@interface favCell : UITableViewCell

@end

@implementation HM_FavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Favoritos",nil);
		self.tabBarItem.image = [UIImage imageNamed:@"fav_unactive"];
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 93) style:UITableViewStylePlain];
	_tableView.backgroundColor = [UIColor colorWithRed:230/255 green:230/255 blue:230/255 alpha:0.2];
	_tableView.separatorColor = [UIColor grayColor];
	_tableView.delegate = self;
	_tableView.dataSource = self;
	_tableView.rowHeight = 38;
	
	[self.view addSubview:_tableView];
	[self loadData];
}

-(void)loadData
{	
	_favRoutes = nil;
	_favRoutes = [HM_AppDelegate sharedAppDelegate].data.favoriteEvents;	
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self loadData];
	[_tableView reloadData];
	//[[HM_AppDelegate sharedAppDelegate] showTitle];
	self.tabBarController.navigationItem.leftBarButtonItem = nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _favRoutes.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
	NSString *cellIdentifier = @"Cell Identifier";
	favCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	cell = nil;
	if (cell==nil) {
		cell = [[favCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.textLabel.text = [_favRoutes objectAtIndex:indexPath.row];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
		cell.backgroundColor = [UIColor clearColor];
		cell.imageView.image = [UIImage imageNamed:@"favorites.png"];
	}
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath 
{
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//	[[HM_AppDelegate sharedAppDelegate] hideTitle];
	HM_DetailViewController *detailView = [[HM_DetailViewController alloc] initWithData:[[HM_AppDelegate sharedAppDelegate].data.events objectForKey:[_favRoutes objectAtIndex:indexPath.row]] andTitle:nil];
	if (detailView != nil)
		[self.navigationController pushViewController:detailView animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) 
	{
		[[HM_AppDelegate sharedAppDelegate].data removeFavoriteEvent:[_favRoutes objectAtIndex:indexPath.row]];
		[tableView reloadData];
	}    
}

@end

@implementation favCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier	
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		self.textLabel.backgroundColor = [UIColor clearColor];
	}
	return self;
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	self.imageView.frame = CGRectMake(10, 8, 20, 20);
	self.backgroundColor = [UIColor clearColor];
}

@end
