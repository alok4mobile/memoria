//
//  HM_SearchResultsViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_SearchResultsViewController.h"
#import "HM_AppDelegate.h"
#import "HM_DetailViewController.h"


@interface HM_SearchResultsViewController ()

@end

@interface searchResultCell : UITableViewCell

@end

@implementation HM_SearchResultsViewController

- (id)initWithData:(NSArray*)data
{
    self = [super init];
    if (self) {
        _data = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.tableView.backgroundColor =  [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:0.9];
	self.tableView.separatorColor = [UIColor grayColor];
	self.tableView.rowHeight = 40;
}



#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSLog(@"count= %i",_data.count);
    return _data.count;
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    searchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell==nil) 
	{
	cell = [[searchResultCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    cell.textLabel.text = [_data objectAtIndex:indexPath.row];
	cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
	cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
	cell.backgroundColor = [UIColor clearColor];
	return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",[_data objectAtIndex:indexPath.row]);
	
	HM_DetailViewController *detailView = [[HM_DetailViewController alloc] initWithData:[[HM_AppDelegate sharedAppDelegate].data.events objectForKey:[_data objectAtIndex:indexPath.row]] andTitle:nil];
	if (detailView != nil)
		[self.navigationController pushViewController:detailView animated:YES];
}

@end

@implementation searchResultCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier	
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		self.textLabel.backgroundColor = [UIColor clearColor];
		
	}
	return self;
	
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	self.backgroundColor = [UIColor clearColor];
	
}

@end


