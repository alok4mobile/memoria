//
//  HM_AboutUsViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_AboutUsViewController.h"

@interface HM_AboutUsViewController ()

@end

@implementation HM_AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	
	[textScroll removeFromSuperview];
	
	NSString *contentOfFile;
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	NSLog(@"localeIdentifier: <%@>", language);
	
	NSString *filePath;
	
	if ([language isEqualToString:@"ca"]) 
	{
		filePath = [[NSBundle mainBundle] pathForResource:@"aboutus_cat" ofType:@"txt"];	
	}
	else 
	{
		filePath = [[NSBundle mainBundle] pathForResource:@"aboutus_es" ofType:@"txt"];  	
	}
	if (filePath) 
	{
		contentOfFile = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
	}
		
	CGSize maximumLabelSize = CGSizeMake(280,9000);
	CGSize expectedLabelSize = [contentOfFile sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:13] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap];   
	CGRect aboutUsFrame;
	aboutUsFrame.size.height = expectedLabelSize.height;
	
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,20,280,aboutUsFrame.size.height)];
	textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
	textLabel.text = contentOfFile;
	textLabel.textAlignment = UITextAlignmentLeft;
	textLabel.numberOfLines = 0;
		
	textScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 20)];
	textScroll.contentSize = CGSizeMake(self.view.frame.size.width, 25 + aboutUsFrame.size.height);
	
	[textScroll addSubview:textLabel];
	[self.view addSubview:textScroll];
	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end