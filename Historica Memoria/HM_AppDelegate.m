//
//  HM_AppDelegate.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_AppDelegate.h"

#import "HM_LandingViewController.h"

#import "HM_ARViewController.h"

#import "HM_FavoritesViewController.h"

#import "HM_CercarViewController.h"

#import "HM_OptionsViewController.h"

#import "HM_poi.h"

@implementation HM_AppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize data = _data;
@synthesize targetPOI = _targetPOI;

@synthesize gps;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    	
	UIViewController *viewController1 = [[HM_LandingViewController alloc] initWithNibName:nil bundle:nil];
	UINavigationController *vc1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
	
	UIViewController *viewController2 = [[HM_ARViewController alloc] initWithEventOnMap:nil];
	UINavigationController *vc2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
	
	UIViewController *viewController3 = [[HM_FavoritesViewController alloc] initWithNibName:@"HM_FavoritesViewController" bundle:nil];
	UINavigationController *vc3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
	
	UIViewController *viewController4 = [[HM_CercarViewController alloc] initWithNibName:@"HM_CercarViewController" bundle:nil];
	UINavigationController *vc4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
	
	UIViewController *viewController5 = [[HM_OptionsViewController alloc] initWithNibName:@"HM_OptionsViewController" bundle:nil];
	UINavigationController *vc5 = [[UINavigationController alloc] initWithRootViewController:viewController5];

	_data = [[HM_DataModel alloc] init];
	
	[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f]];
	[[UIBarButtonItem appearance] setTintColor:[UIColor blackColor]];
	
	
	self.tabBarController = [[UITabBarController alloc] init];
	self.tabBarController.viewControllers = [NSArray arrayWithObjects:vc1, vc2,vc3,vc4,vc5, nil];
	self.tabBarController.delegate = self;
	self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    
    [self startGPS];
    return YES;
}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //NSLog(@"selected %d",tabBarController.selectedIndex);
	
	if (tabBarController.selectedIndex == 0) {

		HM_LandingViewController *viewController1 = [[(UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:0] viewControllers] objectAtIndex:0] ;
				
		// = (UINavigationController*)viewController;
		[viewController1 arPressed];
	}
	
}


+ (HM_AppDelegate *)sharedAppDelegate {
	return (HM_AppDelegate *) [UIApplication sharedApplication].delegate;
}

-(void)clearData {
	
    
    _data = nil;
	_data = [[HM_DataModel alloc] init];	
    
    [self stopGPS];
    [self startGPS];
		
}

- (void) startGPS {
    self.gps = [[ArgolDevice alloc] init];
    [self.gps startCheckingLocation];
    [self loadTargets];
}

- (void) loadTargets {
    double lat;
    double lon;
    
	int tag = 0;
	
	self.targetPOI = [NSMutableArray new];
	
	
	for (HM_EventData *targetData in _data.eventsArray) {
	
		//lat = [targetData.latitude doubleValue];
		//lon = [targetData.longitude doubleValue];
		
		lat = 50.077+(10.1 * rand()/RAND_MAX) * 0.00008 * 2 - 0.00008;
		lon = 14.445+(10.1 * rand()/RAND_MAX) * 0.00008 * 2 - 0.00008;
		
		CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
		ArgolTarget *target = [[ArgolTarget alloc] initWithDevice:self.gps targetLocation:targetLocation];
		target.tag = tag;
		[self.gps.targets addObject:target];
				
		HM_poi *poi = [[HM_poi alloc] init];
		poi.target = target;
		poi.event = targetData;
		
		[self.targetPOI addObject:poi];
		
	++tag;
	}
	
	NSLog(@"targets added: %i",tag);
	[self.gps startCheckingTargets];
}

- (void) stopGPS {
    [self.gps stopCheckingTargets];
}


@end
