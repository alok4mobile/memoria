//
//  HM_DataModel.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_DataModel.h"

@implementation HM_DataModel {
	NSString* _favoritesPath;
}

-(id)init {
	self = [super init];
	
	if (self) {
		// load event data
		_events = [NSMutableDictionary new];
		_eventsArray = [NSMutableArray new];
		_eventsByEventName = [NSMutableDictionary new];
		_eventsByRouteName = [NSMutableDictionary new];
		
		NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
		
		NSString *path;
		
		if ([language isEqualToString:@"ca"]) 
		{
			path = [[NSBundle mainBundle] pathForResource:@"data_cat" ofType:@"plist"]; 
		}
		else if (![language isEqualToString:@"ca"])
		{
			path = [[NSBundle mainBundle] pathForResource:@"data_cast" ofType:@"plist"];  	
		}
		
		NSArray* plistData = [NSArray arrayWithContentsOfFile:path];
		
		for (NSDictionary *value in plistData) // load all name of events from the plist with white spaces and line breaks.
		{
			HM_EventData *data = [[HM_EventData alloc]initWithDictionary:value];
			
			[_events setObject:data forKey:data.name];
			[_eventsArray addObject:data];
			
			for (NSString* eventName in data.events) {
				NSMutableArray* keyValues = [_eventsByEventName objectForKey:eventName];
				if (keyValues == nil) {
					keyValues = [NSMutableArray new];
					[_eventsByEventName setObject:keyValues forKey:eventName];
				}
				[keyValues addObject:data];
			}
		}
		
		for (NSDictionary *value in plistData) {
			HM_EventData *data = [[HM_EventData alloc]initWithDictionary:value];
			NSString* key = data.route;
			NSMutableArray* routekeyValues = [_eventsByRouteName objectForKey:key];
			if (routekeyValues == nil) {
				routekeyValues = [NSMutableArray new];
				[_eventsByRouteName setObject:routekeyValues forKey:key];
				
			}
			[routekeyValues addObject:data];
		}
		
		// load favorites
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
		NSString *documentsDirectory = [paths objectAtIndex:0];
		
		if ([language isEqualToString:@"ca"]) {
			_favoritesPath = [documentsDirectory stringByAppendingPathComponent:@"favs_ca.plist"]; 
		}
		else _favoritesPath = [documentsDirectory stringByAppendingPathComponent:@"favs_es.plist"]; 
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		if ([fileManager fileExistsAtPath: _favoritesPath]) 
		{
			_favoriteEvents = [[NSMutableArray alloc] initWithContentsOfFile: _favoritesPath];
		} 
		else 
		{
			_favoriteEvents = [[NSMutableArray alloc] init];
		}
		
	}
	
	return self;
}

-(void)saveFavorites {
	
	[_favoriteEvents writeToFile: _favoritesPath atomically:YES];
}
-(void)addFavoriteEvent:(HM_EventData *)eventData
{
	[_favoriteEvents addObject:eventData.name];
	[self saveFavorites];
}

-(void)removeFavoriteEvent:(NSString *)eventData
{
	[_favoriteEvents removeObject:eventData];
	[self saveFavorites];
}

-(BOOL)isPageFavorite:(HM_EventData *)eventData
{
	return [_favoriteEvents containsObject:eventData.name];
}


-(NSDictionary *)events {
	return _events;
}

-(NSDictionary *)eventsByEventName {
	return _eventsByEventName;
}

-(NSDictionary *)eventsByRouteName {
	return _eventsByRouteName;
}



-(NSArray *)eventsArray {
	return _eventsArray;
}


-(NSArray *)favoriteEvents {
	return _favoriteEvents;
}

-(NSArray *)routes {
	NSMutableSet *routes = [NSMutableSet new];
	for (HM_EventData *e in self.events.allValues)
	{
		[routes addObject:e.route];
	}
	
	return [routes allObjects];
}



- (NSArray *) eventWithRoute:(NSString *)route {
	
	NSMutableSet *events = [NSMutableSet new];
	for (HM_EventData *e in self.events.allValues){
		if ((route == nil || [e.route isEqualToString:route]) ) {
			[events addObjectsFromArray:e.events];
		}
		
	}
	return [events allObjects];
}

- (NSArray *) RouteWithEvent:(NSString *)event {
	
	NSMutableSet *routes = [NSMutableSet new];
	for (HM_EventData *e in self.events.allValues){
		if (event == nil || [e.events containsObject:event]) {
			[routes addObject:e.route];
		}
		
	}
	return [routes allObjects];
}

- (NSArray *)namesWithEventandRoute:(NSString*)event:(NSString*)route{
	
	NSMutableSet *eventnames = [NSMutableSet new];
	for (HM_EventData *e in self.events.allValues){
		if ((event == nil  || [e.events containsObject:event]) && ( route == nil || [e.route isEqualToString:route])) {
			[eventnames addObject:e.name];
		}
		
	}
	return [eventnames allObjects];
}




- (NSArray*)searchResults:(NSString*)event:(NSString*)route:(NSString*)name
{
	NSMutableSet *eventnames = [NSMutableSet new];
	for (HM_EventData *e in self.events.allValues){
		if ((event == nil  || [e.events containsObject:event]) && ( route == nil || [e.route isEqualToString:route]) && ( name == nil || [e.name isEqualToString:name])) {
			[eventnames addObject:e.name];
		}
		
	}
	return [eventnames allObjects];
}



@end
