//
//  HM_LandingViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 8/14/13.
//
//

#import "HM_LandingViewController.h"
@class topButton;
@interface HM_LandingViewController () {
	ArgolTarget *firstTarget;
	topButton *mainPOIimage;
	topButton *secondPOIimage;
	topButton *thirdPOIimage;
	UILabel *mainEventName;
	UILabel *uglyBlueLabel;
	
}
- (void) targetsUpdated;

@end
@interface tableCell : UITableViewCell

@end

@interface topButton : UIButton
@property (nonatomic, strong) NSString* buttonDescription;

@end


@implementation HM_LandingViewController

@synthesize poiDistanceLabel = _poiDistanceLabel, firstClosestTarget = _firstClosestTarget,secondClosestTarget = _secondClosestTarget, thirdClosestTarget = _thirdClosestTarget;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		//[self.navigationController setNavigationBarHidden:NO];
		self.tabBarItem.image = [UIImage imageNamed:@"menu_unactive"];
		firstTarget.distance = 9999999;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(targetsUpdated) name:@"4mAR_TARGETS_CHECKED" object:nil];
        
	}
    return self;
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:YES];
	if (!_pageScrollisHidden) {
		NSLog(@"reset views here");
		[_pageScroll setContentOffset:CGPointMake(320*resetToPage, 0)];
		
	}
}



-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:YES];
	
	//[self.navigationController setNavigationBarHidden:NO];
	
	//[[HM_AppDelegate sharedAppDelegate] showTitle];
	
	self.title = @"Memòria BCN";
	
	self.tabBarItem.title = NSLocalizedString(@"Inicio",nil);
	
	
	[_topButtons removeAllObjects];
	[_tableViews removeAllObjects];
	_tableViews = nil;
	
	[_topscroll removeFromSuperview];
	[_pageScroll removeFromSuperview];
	[_eventsTable removeFromSuperview];
	
	
	_topscroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
	
	
	_pageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _topscroll.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];//height: self.view.frame.size.height - topScroll.frame.size.height
	_pageScroll.delegate = self;
	_topscroll.backgroundColor = [UIColor colorWithRed:40.0f/255.0f green:40.0f/255 blue:40.0f/255.0f alpha:1.0f]; // gray
	
	//Scroll on top
	
	[_topscroll setShowsHorizontalScrollIndicator:NO]; // getting rid of bottom scroll bar
	self.view.backgroundColor = [UIColor colorWithRed:40.0f/255.0f green:40.0f/255 blue:40.0f/255.0f alpha:1.0f];
	
	views = 0;
	
	float buttonStartPoint = 5;
	float buttonwidth = 65;
	float buttonheight = 40;
	float buttonspacing = 5;
	
	_topButtons = [NSMutableArray new];
	_tableViews	= [NSMutableArray new];
	
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	UIImageView *eventBox = [[UIImageView alloc] initWithFrame:CGRectMake(buttonStartPoint, 5, buttonwidth, buttonheight)];
	
	
	if ([language isEqualToString:@"ca"])
	{
		eventBox.image = [UIImage imageNamed:@"totes-les-epoques"];
	}
	else if (![language isEqualToString:@"ca"])
	{
		eventBox.image = [UIImage imageNamed:@"todas-las-epocas"];
	}
	
	[_topscroll addSubview:eventBox];
	
	int j = 0;
	
	//add event names
	
	NSMutableArray *newEventOrder = [NSMutableArray arrayWithArray:[HM_AppDelegate sharedAppDelegate].data.eventsByEventName.allKeys];
	
	for (NSString* eventName in [HM_AppDelegate sharedAppDelegate].data.eventsByEventName.allKeys) {
		
		if ([eventName isEqualToString:@"Dictadura"] || [eventName isEqualToString:@"Franquismo"]) {
			[newEventOrder replaceObjectAtIndex:2 withObject:eventName];
		}
		
		if ([eventName isEqualToString:@"Segunda República"] || [eventName isEqualToString:@"Segona República"]) {
			[newEventOrder replaceObjectAtIndex:0 withObject:eventName];
		}
		
		if ([eventName isEqualToString:@"Guerra Civil"] || [eventName isEqualToString:@"Guerra Civil"]) {
			[newEventOrder replaceObjectAtIndex:1 withObject:eventName];
		}
		if ([eventName isEqualToString:@"Transición"] || [eventName isEqualToString:@"Transició"]) {
			[newEventOrder replaceObjectAtIndex:3 withObject:eventName];
		}
	}
	
	for (NSString* eventName in newEventOrder) {
		topButton *newButton = [[topButton alloc] initWithFrame:CGRectMake(buttonStartPoint + ((buttonwidth+buttonspacing)*(j+1)), 5, buttonwidth, buttonheight)]; // add this button to scrollview
		
		NSString *buttontitle;
		
		if ([eventName isEqualToString:@"Dictadura"]) {
			buttontitle = @"Franquismo";
		}
		else {
			buttontitle = eventName;
		}
		
		newButton.buttonDescription = eventName;
		
		UILabel	*buttonlabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 60, 40)];
		[buttonlabel removeFromSuperview];
		[newButton setImage:[UIImage imageNamed:@"button_unselected.png"] forState:UIControlStateNormal];
		[newButton setImage:[UIImage imageNamed:@"button_selected.png"] forState:UIControlStateSelected];
		[newButton setTitle:eventName forState:UIControlStateNormal];
		
		buttonlabel.font = [UIFont fontWithName:@"Arial" size:10];
		buttonlabel.textColor = [UIColor whiteColor];
		buttonlabel.backgroundColor = [UIColor clearColor];
		buttonlabel.text = buttontitle;
		buttonlabel.textAlignment = UITextAlignmentCenter;
		buttonlabel.numberOfLines = 0;
		[newButton addSubview:buttonlabel];
		[newButton addTarget:self action:@selector(eventPressed:) forControlEvents:UIControlEventTouchUpInside];
		[_topscroll addSubview:newButton];
		[_topButtons addObject:newButton];
		
		_eventsTable = [[UITableView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*j, 0, self.view.frame.size.width, self.view.frame.size.height-30) style:UITableViewStylePlain];
		_eventsTable.rowHeight = 40;
		_eventsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
		UIColor *tableColor = [UIColor colorWithWhite:0.9 alpha:1.0];
		
		_eventsTable.backgroundColor = tableColor;
		_eventsTable.separatorColor = [UIColor grayColor];
		
		//[_eventsTable removeFromSuperview];
		[_pageScroll addSubview:_eventsTable];
		
		_eventsTable.delegate = self;
		_eventsTable.dataSource = self;
		
		[_tableViews addObject:_eventsTable];
		
		++j;
	}
	
	UIImageView *routeBox = [[UIImageView alloc] initWithFrame:CGRectMake(buttonStartPoint + ((buttonwidth+buttonspacing)*(j+1)), 5, buttonwidth, buttonheight)];
	
	if ([language isEqualToString:@"ca"])
	{
		routeBox.image = [UIImage imageNamed:@"rutes-per-barris"];
	}
	else if (![language isEqualToString:@"ca"])
	{
		routeBox.image = [UIImage imageNamed:@"rutas-por-barrios"];
	}
	[_topscroll addSubview:routeBox];
	
	// add routes
	
	NSMutableArray *newRouteOrder = [NSMutableArray arrayWithArray:[HM_AppDelegate sharedAppDelegate].data.eventsByRouteName.allKeys];
	
	for (NSString* routeName in [HM_AppDelegate sharedAppDelegate].data.eventsByRouteName.allKeys) {
		
		if ([routeName isEqualToString:@"Sant Martí"] || [routeName isEqualToString:@"De Sant Martí"]) {
			[newRouteOrder replaceObjectAtIndex:7 withObject:routeName];
			
		}
		
		if ([routeName isEqualToString:@"Barrio de Gràcia"] || [routeName isEqualToString:@"Barri de Gràcia"]) {
			[newRouteOrder replaceObjectAtIndex:2 withObject:routeName];
			
		}
		
		if ([routeName isEqualToString:@"La Ribera i la Barceloneta"] || [routeName isEqualToString:@"De la Ribera i de la Barceloneta"]) {
			[newRouteOrder replaceObjectAtIndex:5 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Sant Andreu"] || [routeName isEqualToString:@"De Sant Andreu"]) {
			[newRouteOrder replaceObjectAtIndex:6 withObject:routeName];
			
		}
		
		if ([routeName isEqualToString:@"Sants-Montjuïc"] || [routeName isEqualToString:@"De Sants-Montjuïc"]) {
			[newRouteOrder replaceObjectAtIndex:8 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Eixample Derecho"] || [routeName isEqualToString:@"Eixample dret"]) {
			[newRouteOrder replaceObjectAtIndex:0 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Sarrià-Sant Gervasi"] || [routeName isEqualToString:@"De Sarrià-Sant Gervasi"]) {
			[newRouteOrder replaceObjectAtIndex:9 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Les Corts y Eixample izquierdo"] || [routeName isEqualToString:@"De les Corts i l'Eixample esquerre"]) {
			[newRouteOrder replaceObjectAtIndex:4 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Barrio gótico y del Raval"] || [routeName isEqualToString:@"Barri Gòtic i del Raval"]) {
			[newRouteOrder replaceObjectAtIndex:1 withObject:routeName];
			
		}
		if ([routeName isEqualToString:@"Horta- Guinardó"] || [routeName isEqualToString:@"D'Horta- Guinardó"]) {
			[newRouteOrder replaceObjectAtIndex:3 withObject:routeName];
			
		}
		
	}
	
	
	for (NSString* routeName in newRouteOrder)
	{
		topButton *newButton = [[topButton alloc] initWithFrame:CGRectMake(buttonStartPoint + ((buttonwidth+buttonspacing)*(j+2)), 5, buttonwidth, buttonheight)]; // add this button to scrollview
		
		NSString *buttontitle;
		
		if ([routeName isEqualToString:@"Barri Gòtic i del Raval"]) {
			buttontitle = @"Gòtic \nRaval";
		}
		
		else if ([routeName isEqualToString:@"Barri de Gràcia"]) {
			buttontitle = @"Gràcia";
		}
		
		else if ([routeName isEqualToString:@"D'Horta- Guinardó"]) {
			buttontitle = @"Horta- Guinardó";
		}
		else if ([routeName isEqualToString:@"De les Corts i l'Eixample esquerre"]) {
			buttontitle = @"Les Corts Eixample";
		}
		else if ([routeName isEqualToString:@"De la Ribera i de la Barceloneta"]) {
			buttontitle = @"Ribera Barcelone.";
		}
		else if ([routeName isEqualToString:@"De Sant Martí"]) {
			buttontitle = @"Sant Martí";
		}
		else if ([routeName isEqualToString:@"De Sant Andreu"]) {
			buttontitle = @"Sant Andreu";
		}
		
		else if ([routeName isEqualToString:@"De Sants-Montjuïc"]) {
			buttontitle = @"Sants- Montjuïc";
		}
		else if ([routeName isEqualToString:@"De Sarrià-Sant Gervasi"]) {
			buttontitle = @"Sarrià- \nSt. Gervasi";
		}
		
		//lang change
		
		else if ([routeName isEqualToString:@"Barrio gótico y del Raval"]) {
			buttontitle = @"Gòtic \nRaval";
		}
		else if ([routeName isEqualToString:@"Barrio de Gràcia"]) {
			buttontitle = @"Gràcia";
		}
		else if ([routeName isEqualToString:@"Les Corts y Eixample izquierdo"]) {
			buttontitle = @"Les Corts Eixample";
		}
		else if ([routeName isEqualToString:@"La Ribera i la Barceloneta"]) {
			buttontitle = @"Ribera Barcelone.";
		}
		else if ([routeName isEqualToString:@"Sarrià-Sant Gervasi"]) {
			buttontitle = @"Sarrià- \nSt. Gervasi";
		}
		
		else {
			buttontitle = routeName;
		}
		
		newButton.buttonDescription = routeName;
		
		UILabel	*buttonlabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 60, 40)];
		[buttonlabel removeFromSuperview];
		[newButton setImage:[UIImage imageNamed:@"button_unselected.png"] forState:UIControlStateNormal];
		[newButton setImage:[UIImage imageNamed:@"button_selected.png"] forState:UIControlStateSelected];
		[newButton setTitle:routeName forState:UIControlStateNormal];
		
		buttonlabel.font = [UIFont fontWithName:@"Arial" size:10];
		buttonlabel.textColor = [UIColor whiteColor];
		buttonlabel.backgroundColor = [UIColor clearColor];
		buttonlabel.text = buttontitle;
		buttonlabel.textAlignment = UITextAlignmentCenter;
		buttonlabel.numberOfLines = 0;
		[newButton addSubview:buttonlabel];
		[newButton addTarget:self action:@selector(eventPressed:) forControlEvents:UIControlEventTouchUpInside];
		[_topscroll addSubview:newButton];
		[_topButtons addObject:newButton];
		
		_eventsTable = [[UITableView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*j, 0, self.view.frame.size.width, self.view.frame.size.height-30) style:UITableViewStylePlain];
		_eventsTable.rowHeight = 40;
		_eventsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
		UIColor *tableColor = [UIColor colorWithWhite:0.9 alpha:1.0];
		
		_eventsTable.backgroundColor = tableColor;
		_eventsTable.separatorColor = [UIColor grayColor];
		//[_eventsTable removeFromSuperview];
		[_pageScroll addSubview:_eventsTable];
		_eventsTable.delegate = self;
		_eventsTable.dataSource = self;
		
		[_tableViews addObject:_eventsTable];
		
		++j;
	}
	
	
	views = j+1;
		
	
	
	if (_pageScrollisHidden) {
		_pageScroll.hidden = YES;
		//[self arPressed];
	}
	
	
	
	_topscroll.contentSize = CGSizeMake(buttonStartPoint + ((buttonwidth+buttonspacing)*(j+2)), _topscroll.frame.size.height); //scroll size = x start + no. of buttons*button width.
	
	_pageScroll.pagingEnabled = YES;
	_pageScroll.contentSize = CGSizeMake(320*j, _pageScroll.frame.size.height); //scroll size = x start + no. of buttons*button width.
	
	
	if ([language isEqualToString:@"ca"]) // bole toh catalan
	{
		uglyBlueLabel.text = @"  TAMBÉ A PROP TENS...";
	}
	else if (![language isEqualToString:@"ca"]) // bole toh spanish
	{
		uglyBlueLabel.text = @"  CERCA TAMBIÉN TIENES...";
	}
	
	[self.view addSubview:_topscroll];
	[self.view addSubview:_pageScroll];
	
}

- (void) viewDidLoad {
	[super viewDidLoad];
	
	//[self.navigationController setNavigationBarHidden:NO];
#pragma mark AR happens here
	
	arView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, (self.view.frame.size.height - _topscroll.frame.size.height))];
	arView.hidden = NO;
	
#pragma mark first POI
	
	mainPOIimage = [[topButton alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 180)];
	mainPOIimage.backgroundColor = [UIColor clearColor];
	
	[mainPOIimage addTarget:self action:@selector(selectedPOI:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:mainPOIimage];
	
	// add labels onto center image on this transparent thingy
	
	UIView *centerLow = [[UIView alloc] initWithFrame:CGRectMake(20, (mainPOIimage.frame.origin.y + mainPOIimage.frame.size.height)-35, (self.view.frame.size.width) - 40, 25)];
	[centerLow setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
	
	//Main Event Name and description onto transparent thingy
	
	mainEventName = [[UILabel alloc] initWithFrame:CGRectMake(5, (centerLow.frame.size.height/2)-5, centerLow.frame.size.width - 5, centerLow.frame.size.height/2)];
	
	mainEventName.textColor = [UIColor whiteColor];
	mainEventName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
	mainEventName.backgroundColor = [UIColor clearColor];
	[centerLow addSubview:mainEventName];
	
	//Main POI Distance
	
	_poiDistanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 80, centerLow.frame.size.height/2)];
	
	_poiDistanceLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
	_poiDistanceLabel.font = [UIFont fontWithName:@"Arial" size:14];
	_poiDistanceLabel.textColor = [UIColor colorWithRed:171.0f/255.0f green:231.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
	
	// that ugly blue more items label
	
	uglyBlueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 183, self.view.frame.size.width, 19)];
	uglyBlueLabel.backgroundColor = [UIColor colorWithRed:53.0f/255.0f green:168.0f/255.0f blue:223.0f/255.0f alpha:1.0f];
	
	//
	uglyBlueLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
	uglyBlueLabel.textColor = [UIColor whiteColor];
	uglyBlueLabel.textAlignment = UITextAlignmentLeft;
	
	[arView addSubview:uglyBlueLabel];
	
#pragma mark 2nd POI
	
	secondPOIimage = [[topButton alloc] initWithFrame:CGRectMake(0, 205, self.view.frame.size.width/2 - 5, 110)];
	
	[secondPOIimage addTarget:self action:@selector(selectedPOI:) forControlEvents:UIControlEventTouchUpInside];
	
#pragma mark third POI
	thirdPOIimage = [[topButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 + 5, 205, self.view.frame.size.width/2 - 5, 110)];
	
	[thirdPOIimage addTarget:self action:@selector(selectedPOI:) forControlEvents:UIControlEventTouchUpInside];
	
	[arView addSubview:thirdPOIimage];
	[arView addSubview:secondPOIimage];
	[arView addSubview:mainPOIimage];
	[arView addSubview:centerLow];
	[arView addSubview:_poiDistanceLabel];
	[self.view addSubview:arView];
	
	if (returnIndex !=0) {
		[self eventPressed:[_topButtons objectAtIndex:returnIndex-100]];
	}
	
	_pageScrollisHidden = YES;
	
	
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	int index = [_tableViews indexOfObject:tableView];
	topButton *button = [_topButtons objectAtIndex:index];
	NSArray* events = [NSArray new];
	
	if (index <4) {
		events= [[HM_AppDelegate sharedAppDelegate].data.eventsByEventName objectForKey:button.buttonDescription];
	}
	else if (index > 4 || index == 4){
		events= [[HM_AppDelegate sharedAppDelegate].data.eventsByRouteName objectForKey:button.buttonDescription];
	}
	
	return events.count+1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	int index = [_tableViews indexOfObject:tableView];
	topButton *button = [_topButtons objectAtIndex:index];
	return button.buttonDescription;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
	NSUInteger row =indexPath.row;
	
	NSString *cellIdentifier = @"Cell Identifier";
	
	tableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	int index = [_tableViews indexOfObject:tableView];
	topButton *button = [_topButtons objectAtIndex:index];
	NSArray *dataToDisplay = [NSArray new];
	
	if (index <4) {
		dataToDisplay = [[HM_AppDelegate sharedAppDelegate].data.eventsByEventName objectForKey:button.buttonDescription];
		
	}
	else if (index > 4 || index == 4){
		dataToDisplay = [[HM_AppDelegate sharedAppDelegate].data.eventsByRouteName objectForKey:button.buttonDescription];
	}
	cell = nil;
	if (row < dataToDisplay.count) {
		cell = [[tableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		HM_EventData *celldata = [dataToDisplay objectAtIndex:row];
		cell.textLabel.text = celldata.name;
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
		cell.backgroundColor = [UIColor clearColor];
	}
	
	else if (row == dataToDisplay.count){
		
		cell = [[tableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.backgroundColor = [UIColor clearColor];
	}
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	int index = [_tableViews indexOfObject:tableView];
	topButton *button = [_topButtons objectAtIndex:index];
	
	returnIndex = index + 100;
	
	NSArray *dataToDisplay = [NSArray new];
	NSString *title;
	
	if (index <4)
	{
		dataToDisplay = [[HM_AppDelegate sharedAppDelegate].data.eventsByEventName objectForKey:button.buttonDescription];
		title = button.buttonDescription;
	}
	else if (index > 4 || index == 4)
	{
		dataToDisplay = [[HM_AppDelegate sharedAppDelegate].data.eventsByRouteName objectForKey:button.buttonDescription];
		title = nil;
	}
    
	self.title = nil;
	
	//[[HM_AppDelegate sharedAppDelegate] hideTitle];
	HM_DetailViewController *detailView = [[HM_DetailViewController alloc] initWithData:[dataToDisplay objectAtIndex:indexPath.row] andTitle:title];
	[self.navigationController pushViewController:detailView animated:YES];
}

-(void)eventPressed:(topButton *)button
{
	myBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Atrás", nil) style:UIBarButtonItemStylePlain target:self action:@selector(arPressed)];
	
	self.navigationItem.leftBarButtonItem = myBarButtonItem;
	
	
	
	int globalIndex = [_topButtons indexOfObject:button];
	
	resetToPage = globalIndex;
	
	[_pageScroll setContentOffset:CGPointMake(320*globalIndex, 0)];
	
	
	
	_pageScrollisHidden = NO;
	arView.hidden = YES;
	_pageScroll.hidden = NO;
	for (topButton *_prevButton in _topButtons)
	{
		[_prevButton setSelected:NO];
	}
	
	[button setSelected:YES];
	_mainTitle = button.buttonDescription;
	[_eventsTable reloadData];
}

-(void)arPressed {
	
	arView.hidden = NO;
	_pageScroll.hidden = YES;
	
	for (topButton *_prevButton in _topButtons)
	{
		[_prevButton setSelected:NO];
	}
	_pageScrollisHidden = YES;
	//self.tabBarController.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = nil;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (_pageScroll == scrollView) {
		int position = (_pageScroll.contentOffset.x);
		for (UIButton *_prevButton in _topButtons)
		{
			[_prevButton setSelected:NO];
		}
		int page = position/320;
		NSInteger scrollPosition = (position/320*70);
		UIButton *highlightButton = [_topButtons objectAtIndex:page];
		[highlightButton setSelected:YES];
		if (page >2 && page <views-3) {
			[_topscroll setContentOffset:CGPointMake(scrollPosition + 20, 0) animated:YES];
		}
		else if (page <2 || page == 2) {
			[_topscroll setContentOffset:CGPointMake(0, 0)];
		}
		
		else if (page >views-3 || page == views - 3 ) {
			CGFloat startpoint = _topscroll.contentSize.width;
			[_topscroll setContentOffset:CGPointMake(startpoint - 320, 0) animated:YES];	//startpoint 320
		}
	}
}

-(void)selectedPOI:(topButton*)selected {
	
	NSLog(@"selected event: %@",selected.buttonDescription);
	
	[self arPressed];
	
	HM_DetailViewController *detailView = [[HM_DetailViewController alloc] initWithData:[[HM_AppDelegate sharedAppDelegate].data.events objectForKey:selected.buttonDescription] andTitle:nil];
	[self.navigationController pushViewController:detailView animated:YES];
}

-(void)viewDidUnload {
	
	[self setTopscroll:nil];
	[super viewDidUnload];
}

- (void) targetsUpdated {
	
    
    NSArray *sortedTargets = [[HM_AppDelegate sharedAppDelegate].gps.targets sortedArrayUsingSelector:@selector(compareDistance:)];
    
    _firstClosestTarget = [sortedTargets objectAtIndex:0];
    _secondClosestTarget = [sortedTargets objectAtIndex:1];
    _thirdClosestTarget = [sortedTargets objectAtIndex:2];
    
		
	dispatch_sync(dispatch_get_main_queue(), ^{
        NSString *labelText = [NSString stringWithFormat:@"%i m",(int)_firstClosestTarget.distance];
		
        _poiDistanceLabel.text = labelText ;
        [_poiDistanceLabel sizeToFit];
		
		
		HM_EventData *closestPOI;
		HM_EventData *secondPOI;
		HM_EventData *thirdPOI;
		
		for (HM_poi *poi in [HM_AppDelegate sharedAppDelegate].targetPOI) {
			
			if (poi.target.tag == _firstClosestTarget.tag) {
				
				closestPOI = poi.event;
				
			}
			if (poi.target.tag == _secondClosestTarget.tag) {
				
				secondPOI = poi.event;
				
			}
			if (poi.target.tag == _thirdClosestTarget.tag) {
				
				thirdPOI = poi.event;
				
			}
			
		}
		
		if ([UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",closestPOI.photoA]]) {
			[mainPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",closestPOI.photoA]] forState:UIControlStateNormal];
		}
		else [mainPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",closestPOI.photoB]] forState:UIControlStateNormal];
		
		mainPOIimage.buttonDescription = closestPOI.name;
		
		mainEventName.text = closestPOI.name;
		
		
		if ([UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",secondPOI.photoA]]) {
			[secondPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",secondPOI.photoA]] forState:UIControlStateNormal];
		}
		else [secondPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",secondPOI.photoB]] forState:UIControlStateNormal];
		
		secondPOIimage.buttonDescription = secondPOI.name;
		
		if ([UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",thirdPOI.photoA]]) {
			[thirdPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",thirdPOI.photoA]] forState:UIControlStateNormal];
		}
		else [thirdPOIimage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",thirdPOI.photoB]] forState:UIControlStateNormal];
		
		thirdPOIimage.buttonDescription = thirdPOI.name;
		
		
	});
	
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

@implementation topButton

@synthesize buttonDescription;

@end

@implementation tableCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		self.textLabel.backgroundColor = [UIColor clearColor];
	}
	return self;
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	self.backgroundColor = [UIColor clearColor];
}

@end