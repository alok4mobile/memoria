//
//  HM_DetailViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_DetailViewController.h"
#import "HM_AppDelegate.h"
#import "HM_FullPagePOIViewController.h"
#import "HM_ARViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>

@interface HM_DetailViewController ()

@end

@implementation HM_DetailViewController

- (id)initWithData:(HM_EventData *)pageData andTitle:(NSString*)event

{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _data = pageData;
		section = event;
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    	
	// We start the top Image ScrollView
	
	CGFloat scrollviewheight = 220;
	
	arImageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, scrollviewheight)];
	
	UIImage *imageA = [UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",_data.photoA]];
	UIImage *imageB = [UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",_data.photoB]];
	NSLog(@"image A: %@", [NSString stringWithFormat:@"%@_m.jpg",_data.photoA]);
	NSLog(@"image B: %@", [NSString stringWithFormat:@"%@_m.jpg",_data.photoB]);
	
	
	CGFloat pageControlSize = 30;
	
	if (imageB != nil && imageA != nil) {
				
		is2pages = YES;
		
		CGRect frame;
		
		frame.origin.x = 0;
		frame.origin.y = 0;
		frame.size = arImageScrollView.frame.size;
		
		UIButton *arImageA = [[UIButton alloc] initWithFrame:frame];
		[arImageA setImage:imageA forState:UIControlStateNormal];
		[arImageScrollView addSubview:arImageA];
		[arImageA addTarget:self action:@selector(showSurface) forControlEvents:UIControlEventTouchUpInside];
		frame.origin.x = arImageScrollView.frame.size.width;
		
		UIButton *arImageB = [[UIButton alloc] initWithFrame:frame];
		[arImageB setImage:imageB forState:UIControlStateNormal];
		[arImageB addTarget:self action:@selector(showSurface) forControlEvents:UIControlEventTouchUpInside];
		[arImageScrollView addSubview:arImageB];
	 	arImageScrollView.contentSize = CGSizeMake(arImageScrollView.frame.size.width*2, scrollviewheight);
		
	}	
	
	else if (imageB == nil) {
		is2pages = NO;
		CGRect frame;
		
		frame.origin.x = 0;
		frame.origin.y = 0;
		frame.size = arImageScrollView.frame.size;
		
		UIButton *arImageA = [[UIButton alloc] initWithFrame:frame];
		[arImageA setImage:imageA forState:UIControlStateNormal];
		[arImageA addTarget:self action:@selector(showSurface) forControlEvents:UIControlEventTouchUpInside];
		[arImageScrollView addSubview:arImageA];
		arImageScrollView.contentSize = CGSizeMake(arImageScrollView.frame.size.width, scrollviewheight);
	}
	
	else if (imageA == nil) {
		is2pages = NO;
		CGRect frame;
		frame.origin.x = 0;
		frame.origin.y = 0;
		frame.size = arImageScrollView.frame.size;
		
		UIButton *arImageA = [[UIButton alloc] initWithFrame:frame];
		[arImageA setImage:imageB forState:UIControlStateNormal];
		[arImageA addTarget:self action:@selector(showSurface) forControlEvents:UIControlEventTouchUpInside];
		[arImageScrollView addSubview:arImageA];
		arImageScrollView.contentSize = CGSizeMake(arImageScrollView.frame.size.width, scrollviewheight);
	}
	
	arImageScrollView.pagingEnabled = YES;
	arImageScrollView.showsHorizontalScrollIndicator = NO;
	
	arImageScrollView.delegate = self;
	
	_pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-pageControlSize/2, scrollviewheight, pageControlSize, 20)];
	_pageControl.numberOfPages = 2;
	_pageControl.backgroundColor = [UIColor clearColor]; //for now
	
	//View to hold name of event and blah blah doesnt move with left/right scroll but does with up/down
	
	CGFloat frameXOffset = 20;
	CGFloat frameYOffset = 20;
	CGFloat topTextHolderHeight = 20;
	
	//MAP/AR button
	
#pragma mark AR view loads here
	
	CGFloat arButtonwidth = 70;
	CGFloat arButtonheight = 20;
	CGFloat buttonYoffest;
	
	buttonYoffest = _pageControl.frame.origin.y + _pageControl.frame.size.height - 10;
	
	UIButton *mapARButton = [[UIButton alloc] initWithFrame:CGRectMake(frameXOffset, buttonYoffest, arButtonwidth, arButtonheight)];
	[mapARButton setImage:[UIImage imageNamed:@"MAPA_RA_unactive"] forState:UIControlStateNormal];
	[mapARButton setImage:[UIImage imageNamed:@"MAPA_RA_active"] forState:UIControlStateHighlighted];
	mapARButton.backgroundColor = [UIColor clearColor];
	[mapARButton addTarget:self action:@selector(loadPOI) forControlEvents:UIControlEventTouchUpInside];
	
	// transparent box to hold event headers
	
	UIView *topTextHolder = [[UIView alloc]initWithFrame:CGRectMake(frameXOffset, arImageScrollView.frame.size.height - (frameYOffset + topTextHolderHeight), self.view.frame.size.width - (2*frameXOffset), topTextHolderHeight)];
	topTextHolder.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];//alpha 0.3 before
	
	UILabel *leftText = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, (topTextHolder.frame.size.width/2)-3, 20)];
	leftText.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
	leftText.backgroundColor = [UIColor clearColor];
	leftText.textColor = [UIColor colorWithRed:171.0f/255.0f green:231.0f/255.0f blue:245.0f/255.0f alpha:1.0f];// 171 231 245 blue
	leftText.text = section;
	[topTextHolder addSubview:leftText];
	
	UILabel *rightText = [[UILabel alloc] initWithFrame:CGRectMake((topTextHolder.frame.size.width/2)+2, 0, (topTextHolder.frame.size.width/2)-4, 20)];
	rightText.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
	rightText.textColor = [UIColor colorWithRed:207.0f/255.0f green:192.0f/255.0f blue:159.0f/255.0f alpha:1.0f]; // 207 192 159 yellow 
	rightText.backgroundColor = [UIColor clearColor];
	rightText.textAlignment = UITextAlignmentRight;
	rightText.text = _data.route;
	
	[topTextHolder addSubview:rightText];
	
	//facebook sharebutton
	
	CGFloat shareButtonSize = 31;
	CGFloat shareButtonOffset = 282;
	
	UIButton *facebookButton = [[UIButton alloc] initWithFrame:CGRectMake(shareButtonOffset, 10, shareButtonSize, shareButtonSize)];
	[facebookButton setImage:[UIImage imageNamed:@"facebook-icon"] forState:UIControlStateNormal];
	facebookButton.backgroundColor = [UIColor clearColor];
	[facebookButton addTarget:self action:@selector(shareToFacebook) forControlEvents:UIControlEventTouchUpInside];
	//twitter share button
	
	UIButton *twitterButton = [[UIButton alloc] initWithFrame:CGRectMake(shareButtonOffset, 49, shareButtonSize, shareButtonSize)];
	[twitterButton setImage:[UIImage imageNamed:@"twitter-icon"] forState:UIControlStateNormal];
	twitterButton.backgroundColor = [UIColor clearColor];
	[twitterButton addTarget:self action:@selector(shareToTwitter) forControlEvents:UIControlEventTouchUpInside];
	//Favs button
	
	UIButton *favButton = [[UIButton alloc] initWithFrame:CGRectMake(shareButtonOffset, 88, shareButtonSize, shareButtonSize)];
	[favButton setImage:[UIImage imageNamed:@"fav_icon"] forState:UIControlStateNormal];
	[favButton setImage:[UIImage imageNamed:@"fav_icon_s"] forState:UIControlStateSelected];//later change to selected
	favButton.backgroundColor = [UIColor clearColor];
	[favButton addTarget:self action:@selector(favButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	
	
	
	isEventAlreadyFav = [[HM_AppDelegate sharedAppDelegate].data isPageFavorite:_data];
	if (isEventAlreadyFav) {
		[favButton setSelected:YES];
	}
	
	// Content of Data: route header
	
	CGFloat textYoffset = 30 + mapARButton.frame.origin.y;
	
	UILabel *routeHeader = [[UILabel alloc] initWithFrame:CGRectMake(frameXOffset, textYoffset, self.view.frame.size.width - (2*frameXOffset), 50)];//arImageScrollView.frame.size.height+textYoffset
	routeHeader.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
	routeHeader.backgroundColor = [UIColor clearColor];
	routeHeader.text = [_data.name uppercaseString];
	routeHeader.textColor = [UIColor whiteColor];
	routeHeader.numberOfLines = 0;
	[routeHeader sizeToFit];
	
	// Content of Data: route description
	
	NSString *contentString = [NSString stringWithFormat:@"%@ \n\n %@",_data.textC,_data.textL];
	
	// Adjust the label the the new height.
	
	CGSize maximumLabelSize = CGSizeMake(280,9000);
	CGSize expectedLabelSize = [contentString sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:13] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap];   
	CGRect routeDetailFrame;
	routeDetailFrame.size.height = expectedLabelSize.height;
	
	CGFloat routeStart = arImageScrollView.frame.size.height + routeHeader.frame.size.height + 45;
	
	UILabel *routeDetail = [[UILabel alloc] initWithFrame:CGRectMake(20, routeStart, 280, routeDetailFrame.size.height)];
	routeDetail.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
	routeDetail.backgroundColor = [UIColor clearColor];
	routeDetail.text = contentString;
	routeDetail.textAlignment = UITextAlignmentLeft;
	routeDetail.textColor = [UIColor whiteColor];
	routeDetail.numberOfLines = 0;
	
	//Full page ScrollView
	
	UIScrollView *bigPageScrollThing = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	
	bigPageScrollThing.contentSize = CGSizeMake(self.view.frame.size.width, routeStart + routeDetail.frame.size.height + 50);
	bigPageScrollThing.backgroundColor = [UIColor colorWithRed:53.0f/255.0f green:168.0f/255.0f blue:223.0f/255.0f alpha:1.0f]; // 1 , 167 , 225 blue background
	[bigPageScrollThing addSubview:arImageScrollView];
	
	if (imageB != nil && imageA != nil) [bigPageScrollThing addSubview:_pageControl];
	
	[bigPageScrollThing addSubview:mapARButton];	
	
	[bigPageScrollThing addSubview:topTextHolder];
	[bigPageScrollThing addSubview:facebookButton];
	[bigPageScrollThing addSubview:favButton];
	[bigPageScrollThing addSubview:twitterButton];
	[bigPageScrollThing addSubview:routeHeader];
	[bigPageScrollThing addSubview:routeDetail];
	
	[self.view addSubview:bigPageScrollThing];
	
}

-(void)loadPOI {
	HM_ARViewController *arView = [[HM_ARViewController alloc] initWithEventOnMap:_data];
	[self.navigationController pushViewController:arView animated:YES];
}


-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)showSurface {
	
	HM_FullPagePOIViewController *fullPage = [[HM_FullPagePOIViewController alloc] initWithImageA:_data.photoA ImageB:_data.photoB];
	
	fullPage.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:fullPage animated:YES];
	
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	CGFloat pageWidth = arImageScrollView.frame.size.width;
	int page = floor((arImageScrollView.contentOffset.x - pageWidth /2) /pageWidth) +1;
	_pageControl.currentPage = page;
}

-(void)shareToFacebook {
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	
	
	SLComposeViewController *SLCcompose = [[SLComposeViewController alloc] init];
	SLCcompose = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
	
	//spanish: Rutas por barrios de Barcelona descubriendo los espacios de memoria histórica.
	//catalan: Rutes per barris de Barcelona descobrint els espais de memòria històrica
	
	if ([language isEqualToString:@"ca"])
	{
		[SLCcompose setInitialText:@"Rutes per barris de Barcelona descobrint els espais de memòria històrica."];
	}
	else
	{
		[SLCcompose setInitialText:@"Rutas por barrios de Barcelona descubriendo los espacios de memoria histórica."];
	}

	[self presentViewController:SLCcompose animated:YES completion:NULL];
	
	
}

-(void)shareToTwitter {
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	TWTweetComposeViewController *tweet = [[TWTweetComposeViewController alloc] init];
	if ([language isEqualToString:@"ca"])
	{
		[tweet setInitialText:@"Rutes per barris de Barcelona descobrint els espais de memòria històrica."];
	}
	else
	{
		[tweet setInitialText:@"Rutas por barrios de Barcelona descubriendo los espacios de memoria histórica."];
	}
	[self presentViewController:tweet animated:YES completion:NULL];

}


-(void)favButtonPressed:(id)sender {
	
	isEventAlreadyFav = [[HM_AppDelegate sharedAppDelegate].data isPageFavorite:_data];
	
	if (isEventAlreadyFav) {
		[self removeEvent];
		[sender setSelected:NO];
		
	}
	else if	(!isEventAlreadyFav){
		[self addevent];
		[sender setSelected:YES];
		
	}	
}

-(void)addevent {
	[[HM_AppDelegate sharedAppDelegate].data addFavoriteEvent:_data];
}

-(void)removeEvent {
	[[HM_AppDelegate sharedAppDelegate].data removeFavoriteEvent:_data.name];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    self.view = nil;
}


@end
