//
//  HM_SecondViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HM_poi.h"
#import <CoreLocation/CoreLocation.h>
#import "HM_DetailViewController.h"

#define METERS_PER_MILE 1609.344
// distance to filter for radar

@class ArgolTarget;

@interface HM_ARViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate> {
	NSArray *arbuttons;
	UITableView *poiList;
	UIImageView *visor;
	UIView *radarView;
	UIView *poiView;
	MKMapView *poiMap;
	BOOL mapisPoint;
	HM_EventData *loadWithMap;
	NSArray *filtered;
	UIView *headerView;
	UILabel *poiName;
	//UISlider *distanceSlider;
	int	distanceFilter;
}
- (id)initWithEventOnMap:(HM_EventData*)event;

@end
