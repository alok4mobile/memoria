//
//  HM_OptionsViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HM_AppDelegate.h"


@interface HM_OptionsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {

	UITableView *_settingsTable;
	
}

@end
