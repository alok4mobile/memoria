//
//  HM_DetailViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HM_EventData.h"

@interface HM_DetailViewController : UIViewController <UIScrollViewDelegate>{
	HM_EventData *_data;
	UIScrollView *arImageScrollView;
	UIPageControl *_pageControl;
	BOOL isEventAlreadyFav;
	BOOL is2pages;
	
	NSString *section;
}

- (id)initWithData:(HM_EventData *)pageData andTitle:(NSString*)event;

@end
