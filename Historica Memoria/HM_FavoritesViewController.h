//
//  HM_FavoritesViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HM_DetailViewController.h"
#import "HM_EventData.h"

@interface HM_FavoritesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

-(void)loadData;

@end
