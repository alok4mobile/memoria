//
//  HM_OptionsViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_OptionsViewController.h"
#import "HM_AboutUsViewController.h"

@interface HM_OptionsViewController ()

@end

@implementation HM_OptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Opciones",nil);
    self.tabBarItem.image = [UIImage imageNamed:@"Opcions_unactive"];
	self.tabBarController.navigationItem.leftBarButtonItem = nil;
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"05-background"]];
	
	_settingsTable = [[UITableView alloc]initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 280) style:UITableViewStyleGrouped];
	_settingsTable.backgroundColor = [UIColor clearColor];
	_settingsTable.backgroundView = nil;
	_settingsTable.dataSource= self;
	_settingsTable.delegate = self;
	_settingsTable.scrollEnabled = NO;
	_settingsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
	[self.view addSubview:_settingsTable];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
	NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }

	UILabel *headerView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
	headerView.backgroundColor = [UIColor clearColor];
	headerView.textColor = [UIColor whiteColor];
	headerView.text = sectionTitle;
	headerView.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
	return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) return 2;
	if (section == 1) return 1;
	
	else return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case(0):
            return @"Idiomes";
            break;
        case(1):
            return @"Altres...";
            break;
            }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = [ NSString stringWithFormat: @"%d:%d", [ indexPath indexAtPosition: 0 ], [ indexPath indexAtPosition:1 ]];
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	UITableViewCell *cell = [ tableView dequeueReusableCellWithIdentifier: CellIdentifier];
	
	if (cell == nil) {
		cell = [[ UITableViewCell alloc ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
		switch ([ indexPath indexAtPosition: 0]) {
			case(0):
				
				switch([ indexPath indexAtPosition: 1]) {
					
					case(0):
					{
						if ([language isEqualToString:@"ca"]) {
							cell.accessoryType = UITableViewCellAccessoryCheckmark;
						}
						cell.textLabel.text = @"Català"; // OS3
					}
						break;
				
					case(1):
					{
						if (![language isEqualToString:@"ca"]) {
						cell.accessoryType = UITableViewCellAccessoryCheckmark;
					}
						cell.textLabel.text = @"Castellà"; // OS3
					}
						break;
							
				}
				
				break;
			
			case(1):
				
				switch ([ indexPath indexAtPosition: 1 ]) {
					
					case(0):
					{
						cell.textLabel.text = @"Crèdits"; // OS3
						cell.selectionStyle = UITableViewCellSelectionStyleGray;
					}
						break;
				}
				break;
			
		}
	}
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	
	switch ([ indexPath indexAtPosition: 0]) {
			
			
		case(0):
			
			switch([ indexPath indexAtPosition: 1]) {
					
				case(0):
				{	
					
					UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:indexPath];
					NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:indexPath.section];
					
					UITableViewCell *cell2 = [tableView cellForRowAtIndexPath:indexPath1];
					if (cell1.selected) {
						cell1.accessoryType = UITableViewCellAccessoryCheckmark;
					cell2.accessoryType = UITableViewCellAccessoryNone;
					}
					[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"ca", nil] forKey:@"AppleLanguages"]; //switching to Catalan locale
					[[NSUserDefaults standardUserDefaults] synchronize];
					[[HM_AppDelegate sharedAppDelegate] clearData];				
				}
					break;
					
				case(1):
				{
					
					UITableViewCell *cell2 = [tableView cellForRowAtIndexPath:indexPath];
					NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
					UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:indexPath1];
					if (cell2.selected) {
						cell2.accessoryType = UITableViewCellAccessoryCheckmark;
						cell1.accessoryType = UITableViewCellAccessoryNone;
					}
					[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"es", nil] forKey:@"AppleLanguages"]; //switching to Spanish locale
					[[NSUserDefaults standardUserDefaults] synchronize];

				[[HM_AppDelegate sharedAppDelegate] clearData];	
				}
					break;
					
					
			}
			
			break;
			
		case(1):
			
			switch ([ indexPath indexAtPosition: 1 ]) {
					
				case(0):
				{	
					//	[[HM_AppDelegate sharedAppDelegate] hideTitle];
					HM_AboutUsViewController *aboutUs = [[HM_AboutUsViewController alloc] init];
					[self.navigationController pushViewController:aboutUs animated:YES];
				}
					break;
			}
			break;
	}

}

-(void)viewWillAppear:(BOOL)animated {
	self.tabBarController.navigationItem.leftBarButtonItem = nil;
	//[[HM_AppDelegate sharedAppDelegate] showTitle];
	[_settingsTable deselectRowAtIndexPath:[_settingsTable indexPathForSelectedRow] animated:YES];
}

@end
