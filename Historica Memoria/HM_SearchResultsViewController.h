//
//  HM_SearchResultsViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HM_DataModel.h"

@interface HM_SearchResultsViewController : UITableViewController {
	NSArray *_data;
}
- (id)initWithData:(NSArray*)data;
@end
