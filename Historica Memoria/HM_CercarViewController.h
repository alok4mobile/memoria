//
//  HM_CercarViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HM_CercarViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

{
	NSString *_district;
	NSString *_chronological;
	NSString *_localization;
	NSString *_tempString;
	NSMutableArray *searchData;
	UITableView *_tableSearchPage;
	UITableView *_hiddenTable;
	CGFloat hiddentableOffset;
	int caseKey;
}

@end
