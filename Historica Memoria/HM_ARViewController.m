//
//  HM_SecondViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_ARViewController.h"
#import "Argol.h"
#import "HM_AppDelegate.h"

@interface HM_ARViewController () {
	int selectedPOI;
	HM_poi *selectedTarget;
    BOOL startLoading;
	
}
@property (nonatomic, strong) ArgolView *gpsView;
- (void) targetsUpdated;

@end

@interface ARtableCell : UITableViewCell

@end

@interface MapViewAnnotation : NSObject <MKAnnotation> {
	NSString *title;
	CLLocationCoordinate2D coordinate;
}

@property (nonatomic,copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id)initWithTitle:(NSString*)ttl andcood:(CLLocationCoordinate2D)c2d;
@end

@implementation HM_ARViewController

@synthesize gpsView;

- (id)initWithEventOnMap:(HM_EventData*)event
{
    self = [super init];
    if (self) {
		self.title = NSLocalizedString(@"Mapa/AR", @"Mapa/AR");
		self.tabBarItem.image = [UIImage imageNamed:@"map_RA_unactive"];
		self.tabBarController.navigationItem.leftBarButtonItem = nil;
		loadWithMap = event;
		selectedPOI = 99;
		headerView.hidden = YES;
		distanceFilter = 500;
	}
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLoad {
	
	[super viewDidLoad];
	
#pragma mark language initialization
	
	
	UIImage *list_on;
	UIImage *map_on;
	UIImage *radar_on;
	
	UIImage *list_off;
	UIImage *map_off;
	UIImage *radar_off;
	
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	if ([language isEqualToString:@"ca"])
	{
		list_on = [UIImage imageNamed:@"03-list-button_CAT_OFF"];
		map_on = [UIImage imageNamed:@"04-map-button_CAT_OFF"];
		radar_on = [UIImage imageNamed:@"02-radar-button_CAT_OFF"];
		
		list_off = [UIImage imageNamed:@"03-list-button_CAT_ON"];
		map_off = [UIImage imageNamed:@"04-map-button_CAT_ON"];
		radar_off = [UIImage imageNamed:@"02-radar-button_CAT_ON"];
	}
	else if (![language isEqualToString:@"ca"])
	{
		list_on = [UIImage imageNamed:@"03-list-button_ESP_OFF"];
		map_on = [UIImage imageNamed:@"04-map-button_ESP_OFF"];
		radar_on = [UIImage imageNamed:@"02-radar-button_ESP_OFF"];
		
		list_off = [UIImage imageNamed:@"03-list-button_ESP_ON"];
		map_off = [UIImage imageNamed:@"04-map-button_ESP_ON"];
		radar_off = [UIImage imageNamed:@"02-radar-button_ESP_ON"];
	}
	
	
#pragma mark AR View and buttons
	
	self.gpsView = [[ArgolView alloc] initWithFrame:self.view.frame];
	
	
	CGFloat buttonSize = 50;
	CGFloat buttonStart = 310;
	CGFloat menuButtonOffset = 60;
	
	
	UIButton *radarButton = [[UIButton alloc] initWithFrame:CGRectMake(75, buttonStart, buttonSize,buttonSize)];
	[radarButton setImage:radar_off forState:UIControlStateNormal];
	[radarButton setImage:radar_on forState:UIControlStateSelected];
	[radarButton addTarget:self action:@selector(selectedMenu:) forControlEvents:UIControlEventTouchUpInside];
	radarButton.selected = YES;
	
	
	UIButton *listButton = [[UIButton alloc] initWithFrame:CGRectMake(radarButton.frame.origin.x + menuButtonOffset, buttonStart, buttonSize, buttonSize)];
	[listButton setImage:list_off forState:UIControlStateNormal];
	[listButton setImage:list_on forState:UIControlStateSelected];
	[listButton addTarget:self action:@selector(selectedMenu:) forControlEvents:UIControlEventTouchUpInside];
	
	UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(listButton.frame.origin.x + menuButtonOffset, buttonStart, buttonSize, buttonSize)];
	[mapButton setImage:map_off forState:UIControlStateNormal];
	[mapButton setImage:map_on forState:UIControlStateSelected];
	[mapButton addTarget:self action:@selector(selectedMenu:) forControlEvents:UIControlEventTouchUpInside];
	
	arbuttons = [[NSArray alloc] initWithObjects:radarButton,listButton,mapButton, nil];
	
#pragma mark RADAR in AR view
	
	CGFloat radaroffsetx = 40;
	CGFloat radaroffsety = 55;
	
	radarView = [[UIImageView alloc] initWithFrame:CGRectMake(radaroffsetx, radaroffsety, self.view.frame.size.width - (2*radaroffsetx), self.view.frame.size.width - (2*radaroffsetx))];
	radarView.backgroundColor = [UIColor clearColor];
	
	poiView = [[UIView alloc] initWithFrame:radarView.frame];
	poiView.backgroundColor = [UIColor clearColor];
	
	
	visor = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, radarView.frame.size.width, radarView.frame.size.height)];
	visor.image = [UIImage imageNamed:@"RA-visor"];
	visor.tag = 99;
	
	
	CGFloat rangeoffsetx = 47;
	CGFloat rangebottom = 90;
	
	UIImageView *whiteRange = [[UIImageView alloc] initWithFrame:CGRectMake(rangeoffsetx, 9, visor.frame.size.width - (2*rangeoffsetx) - 7, rangebottom)];
	whiteRange.image = [UIImage imageNamed:@"RA-white range"];
	
	[radarView addSubview:visor];
	[radarView addSubview:whiteRange];
	
	
#pragma mark List of POIs in AR
	
	CGFloat tablestartx = 30;
	CGFloat tablestarty = 50;
	
	poiList = [[UITableView alloc] initWithFrame:CGRectMake(tablestartx, tablestarty, self.view.frame.size.width - (2*tablestartx), buttonStart - tablestarty - 5) style:UITableViewStylePlain];
	poiList.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.8];
	poiList.hidden = YES;
	poiList.rowHeight = 30;
	poiList.delegate = self;
	poiList.dataSource = self;
	
#pragma mark Top location holder
	
	headerView = [[UIView alloc] initWithFrame:CGRectMake(30, 15, self.view.frame.size.width - 60, 35)];
	headerView.backgroundColor = [UIColor blackColor];
	headerView.alpha = 0.5;
	headerView.hidden = YES;
	poiName = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, headerView.frame.size.width - 20, 35)];
	poiName.backgroundColor = [UIColor clearColor];
	poiName.textColor = [UIColor whiteColor];
	poiName.font =[UIFont fontWithName:@"HelveticaNeue" size:13];
	poiName.textAlignment = UITextAlignmentCenter;
	[headerView addSubview:poiName];
	
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topHolderPressed)];
	tap.numberOfTapsRequired = 1;
	[headerView addGestureRecognizer:tap];
	
#pragma mark Map view
	
	poiMap = [[MKMapView alloc] initWithFrame:self.view.frame];
	poiMap.delegate = self;
	poiMap.hidden = YES;
	
	[self.view addSubview:self.gpsView];
	[self.view addSubview:radarView];
	[self.view addSubview:poiMap];
	[self.view addSubview:poiList];
	[self.view addSubview:poiView];
	[self.view addSubview:headerView];
		
	[self.view addSubview:radarButton];
	[self.view addSubview:listButton];
	[self.view addSubview:mapButton];
	if (loadWithMap) {
		[self loadMapPOI];
	}
	
}

-(void)topHolderPressed{
	HM_DetailViewController *detailPage = [[HM_DetailViewController alloc] initWithData:selectedTarget.event andTitle:nil];
	[self.navigationController pushViewController:detailPage animated:YES];
}

#pragma mark Menu buttons pressed

-(void)selectedMenu:(UIButton*)pressedButton
{
	for (UIButton *selectedButton in arbuttons) {
		selectedButton.selected = NO;
	}
	
	int index = [arbuttons indexOfObject:pressedButton];
	
	if (index == 1) {
		// POIs list
		poiList.hidden = NO;
		pressedButton.selected = YES;
		radarView.hidden = YES;
		poiMap.hidden = YES;
		poiView.hidden = YES;
		headerView.hidden = YES;
		//topMapButton.selected = NO;
		//topARbutton.selected = NO;
	}
	else if (index == 0){
		//RADAR view
		
		poiList.hidden = YES;
		pressedButton.selected = YES;
		poiMap.hidden = YES;
		visor.tag = 99;
		radarView.hidden = NO;
		poiView.hidden = NO;
		//topMapButton.selected = NO;
		//topARbutton.selected = YES;
	}
	else if (index == 2){
		//MAP view
		poiList.hidden = YES;
		pressedButton.selected = YES;
		radarView.hidden = YES;
		poiMap.hidden = NO;
		poiView.hidden = YES;
		//topMapButton.selected = YES;
		//topARbutton.selected = NO;
		headerView.hidden = YES;
		[self selectedMap:pressedButton];
	}
	
}

#pragma mark Map button pressed

-(void)loadMapPOI{
	
	if (loadWithMap) {
		
		poiView.hidden = YES;
		
		for (HM_poi *poi in [HM_AppDelegate sharedAppDelegate].targetPOI ) {
			
			if (poi.event == loadWithMap) {
				
				NSMutableArray *annotationsToRemove = [poiMap.annotations mutableCopy] ;
				[poiMap removeAnnotations:annotationsToRemove];
				
				mapisPoint = YES;
				
				CLLocationCoordinate2D location;
				location.latitude = (double)[poi.event.latitude doubleValue];
				location.longitude = (double)[poi.event.longitude doubleValue];
				MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:poi.event.name andcood:location];
				
				[poiMap addAnnotation:annotation];
				poiList.hidden = YES;
				poiMap.hidden = NO;
				radarView.hidden = YES;
				for (UIButton *otherButtons in arbuttons) {
					otherButtons.selected = NO;
				}
				UIButton *restoreMapButton = [arbuttons objectAtIndex:2];
				restoreMapButton.selected = YES;
				
			}
			
		}
		
	}
	
}

-(void)selectedMap:(UIButton*)pressedButton {
	
	NSMutableArray *annotationsToRemove = [poiMap.annotations mutableCopy] ;
	[poiMap removeAnnotations:annotationsToRemove];
	mapisPoint = NO;
	
	//topMapButton.selected = YES;
	//topARbutton.selected = NO;
	poiList.hidden = YES;
	radarView.hidden = YES;
	poiMap.hidden = NO;
	
	for (HM_poi *poi in [HM_AppDelegate sharedAppDelegate].targetPOI ) {
		CLLocationCoordinate2D location;
		location.latitude = [poi.event.latitude doubleValue];
		location.longitude = [poi.event.longitude doubleValue];
		
		MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:poi.event.name andcood:location];
		[poiMap addAnnotation:annotation];
	}
	
	CLLocationCoordinate2D zoomLocation;
	zoomLocation.latitude = 41.404;
	zoomLocation.longitude =2.162;
	
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,3.0*METERS_PER_MILE, 3.0*METERS_PER_MILE);
	MKCoordinateRegion adjustedRegion = [poiMap regionThatFits:viewRegion];
	
	[poiMap setRegion:adjustedRegion animated:YES];
	
	
}

#pragma mark AR pressed

-(void)selectedAR:(UIButton*)pressedButton {
	
	//topMapButton.selected = NO;
	//topARbutton.selected = YES;
	radarView.hidden = NO;
	poiList.hidden = YES;
	poiMap.hidden = YES;
	
	for (UIButton *otherButtons in arbuttons) {
		otherButtons.selected = NO;
	}
	
	UIButton *selectedButton = [arbuttons objectAtIndex:0];
	selectedButton.selected = YES;
	
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [HM_AppDelegate sharedAppDelegate].targetPOI.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *cellIdentifier = @"Cell Identifier";
	
	ARtableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	// NSArray *sortedTargets = [[HM_AppDelegate sharedAppDelegate].gps.targets sortedArrayUsingSelector:@selector(compareDistance:)];
	
	NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"target" ascending:YES selector:@selector(compareDistance:)];
	NSArray* sortedArray = [[HM_AppDelegate sharedAppDelegate].targetPOI sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	HM_poi *poi = [sortedArray objectAtIndex:indexPath.row];
	
	cell = [[ARtableCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
	
	cell.textLabel.text = poi.event.name;
	cell.contentView.backgroundColor = [UIColor clearColor];
	cell.backgroundColor = [UIColor clearColor];
	NSString *distance = [NSString stringWithFormat:@"%i m",(int)poi.target.distance];
	cell.detailTextLabel.text = distance;
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSMutableArray *annotationsToRemove = [poiMap.annotations mutableCopy] ;
	[poiMap removeAnnotations:annotationsToRemove];
	
	mapisPoint = YES;
	
	NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"target" ascending:YES selector:@selector(compareDistance:)];
	NSArray* sortedArray = [[HM_AppDelegate sharedAppDelegate].targetPOI sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	HM_poi *poi = [sortedArray objectAtIndex:indexPath.row];
	
	
	//HM_poi *poi = [[HM_AppDelegate sharedAppDelegate].targetPOI objectAtIndex:indexPath.row];
	
	CLLocationCoordinate2D location;
	location.latitude = (double)[poi.event.latitude doubleValue];
	location.longitude = (double)[poi.event.longitude doubleValue];
	
	MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:poi.event.name andcood:location];
	
	[poiMap addAnnotation:annotation];
	
	poiList.hidden = YES;
	poiMap.hidden = NO;
	//topMapButton.selected = YES;
	
	for (UIButton *otherButtons in arbuttons) {
		otherButtons.selected = NO;
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	UIButton *restoreMapButton = [arbuttons objectAtIndex:2];
	restoreMapButton.selected = YES;
}

#pragma mark set up GPS points for POIs

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(MapViewAnnotation<MKAnnotation>*)annotation {
	
	MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:nil];
	annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
	
	annotationView.enabled = YES;
	annotationView.canShowCallout = YES;
	annotationView.animatesDrop = YES;
	//annotationView.image = [UIImage imageNamed:@"RA-POI-icon"]; // change image when you have one.
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	
	//NSLog(@"POI Name: %@",annotation.title);
	[rightButton setTitle:annotation.title forState:UIControlStateNormal];
	rightButton.titleLabel.text = annotation.title;
	[rightButton addTarget:self action:@selector(showDetailEvent:) forControlEvents:UIControlEventTouchUpInside];
	annotationView.rightCalloutAccessoryView = rightButton;
	
	return annotationView;
}

-(void)showDetailEvent:(UIButton*)sender {
	
	NSLog(@"POI Name: %@",sender.currentTitle);
	
	HM_DetailViewController *detailView = [[HM_DetailViewController alloc] initWithData:[[HM_AppDelegate sharedAppDelegate].data.events objectForKey:sender.currentTitle] andTitle:nil];
	if (detailView != nil)
		[self.navigationController pushViewController:detailView animated:YES];
}

-(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
	
	if (mapisPoint) {
		
		MKAnnotationView *annotationView = [views objectAtIndex:0];
		id <MKAnnotation> mp = [annotationView annotation];
		MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1500, 1500);
		[mapView setRegion:region animated:YES];
		[mapView selectAnnotation:mp animated:YES];
	}
	
	
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:YES];
	
	// remove all the previous pois from Radar.
	
//	for (UIView *removeButton in poiView.subviews){
//	
//		if (removeButton) {
//		[removeButton removeFromSuperview];
//		}
//	}
	
    startLoading = YES;
    	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkForGPS) name:@"4mAR_TARGETS_CHECKED" object:nil];
	
}

-(void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:YES];
    startLoading = NO;
   // filtered = nil;
    NSLog(@"stop GPS from AR");
	
//	for (ArgolTarget *removeTarget in filtered) { // remove targets that are outside range.
//		UIView *removeButton = (UIView*)[poiView viewWithTag:removeTarget.tag];
//		[removeButton removeFromSuperview];
//	}
//
	
	
}

#pragma mark AR happens here

-(void) checkForGPS {

    if (startLoading) {
        [self targetsUpdated];
           // NSLog(@"start GPS again");
    }
    

}

- (void) targetsUpdated {
	
	NSArray *updatedResults = [NSArray new];
	
	if (loadWithMap) {
		
		for (HM_poi *poi in [HM_AppDelegate sharedAppDelegate].targetPOI ) {
			
			if (poi.event == loadWithMap) {
				
				
				int index = [[HM_AppDelegate sharedAppDelegate].targetPOI indexOfObject:poi];
				
				NSRange oneObject;
				oneObject.location = index;
				oneObject.length = 1;
				
				NSArray *array = [HM_AppDelegate sharedAppDelegate].gps.targets;
				
				updatedResults = [array subarrayWithRange:oneObject];
				
			}}}
	
	else  if (loadWithMap == nil)
	{
		NSArray *array = [[[HM_AppDelegate sharedAppDelegate].gps.targets filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"distance < %i",distanceFilter]] sortedArrayUsingSelector:@selector(compareDistance:)];
		//NSLog(@"distance filter = %i",distanceFilter);
		NSRange objectsInRange;
		objectsInRange.location = 0;
		
		if (array.count>7) {
			objectsInRange.length = 7; // shortlist to 7 POIs
		}
		else objectsInRange.length = array.count;
		
		updatedResults = [array subarrayWithRange:objectsInRange];
	}
			
	
	NSMutableArray *itemsToBeAdded = [updatedResults mutableCopy]; //updated results = current in range
	NSMutableArray *itemsToBeRemoved = [filtered mutableCopy]; // filtered = already in view
	
	for (ArgolTarget *target in filtered) {
		
		if ([updatedResults containsObject:target]) {
			[itemsToBeAdded removeObject:target];
		}
		
	}
	//itemstobeadded = new ones only
	
	for (ArgolTarget *target in filtered) {
		
		if ([updatedResults containsObject:target]) {
			[itemsToBeRemoved removeObject:target];
		}
		
	}
	
	
	for (ArgolTarget *removeTarget in itemsToBeRemoved) { // remove targets that are outside range.
		UIView *removeButton = (UIView*)[poiView viewWithTag:removeTarget.tag];
		[removeButton removeFromSuperview];
	}
	
	//remove from previous view
		
	
	for (ArgolTarget *addtarget in itemsToBeAdded) { // add targets after updation
		
		double poiX;
		double poiY;
		double distance;
		
		CGFloat startPointX = visor.frame.origin.x + (visor.frame.size.width/2);
		CGFloat startPointY = visor.frame.origin.y + (visor.frame.size.height/2);
		
		distance = addtarget.distance;
		double trueDirection = (addtarget.azimuth - addtarget.device.currHeading.trueHeading)*0.01745; // degrees to radians
		
		UIButton *poi;
		poi = (UIButton*)[poiView viewWithTag:addtarget.tag];
		
		if (poi) {
			[poi removeFromSuperview];
		}
		
		poi = [[UIButton alloc] init];
		
		//poi.opaque = YES;
		poi.tag = addtarget.tag;
		[poi setImage:[UIImage imageNamed:@"RA-POI-icon"] forState:UIControlStateNormal]  ;
		poi.backgroundColor = [UIColor clearColor];
		[poi addTarget:self action:@selector(showPOILabel:) forControlEvents:UIControlEventTouchUpInside];
		
		poiX = (distance*120*sin(trueDirection))/distanceFilter;
		poiY = (distance*120*cos(trueDirection))/distanceFilter;
		
		poi.frame =	CGRectMake(startPointX + poiX - 15,startPointY - poiY - 15, 30, 30);
		[poiView addSubview:poi];
		
		NSLog(@"added POI #%i",addtarget.tag);
	}
		
	dispatch_sync(dispatch_get_main_queue(), ^{
		
		for (ArgolTarget *addtarget in updatedResults) {
					
			double poiX;
			double poiY;
			double distance;
			
			CGFloat startPointX = visor.frame.origin.x + (visor.frame.size.width/2);
			CGFloat startPointY = visor.frame.origin.y + (visor.frame.size.height/2);
			
			distance = addtarget.distance;
			double trueDirection = (addtarget.azimuth - addtarget.device.currHeading.trueHeading)*0.01745; // degrees to radians
			
			UIButton *poi = [[UIButton alloc] init];
			
			poi = (UIButton*)[poiView viewWithTag:addtarget.tag];
						
			poi.backgroundColor = [UIColor clearColor];
			
			if (poi.tag == selectedPOI) {
				[poi setImage:[UIImage imageNamed:@"pict_AR_selected"] forState:UIControlStateNormal]  ;
			}
			else {				
				if ([poi respondsToSelector:@selector(setImage:forState:)]){
				[poi setImage:[UIImage imageNamed:@"pict_AR"] forState:UIControlStateNormal];
				}
				
			}
			poiX = (distance*120*sin(trueDirection))/distanceFilter;
			poiY = (distance*120*cos(trueDirection))/distanceFilter;
			
			poi.frame =	CGRectMake(startPointX + poiX - 15,startPointY - poiY - 15, 30, 30);
			[poiView addSubview:poi];
		}
	});
	
	filtered = [updatedResults copy];
	
	updatedResults = nil;
	itemsToBeRemoved = nil;
	itemsToBeAdded = nil;
}

-(void)showPOILabel:(UIButton*)button {
	
	NSLog(@"Button pressed #%i",button.tag);
	selectedPOI = button.tag;
	
	
	selectedTarget = [HM_poi new];
	
	
	for (HM_poi *targetData in [HM_AppDelegate sharedAppDelegate].targetPOI) {
		if (targetData.target.tag == button.tag) {
			selectedTarget = targetData;
		}
	}
	if (selectedTarget) {
		headerView.hidden = NO;
		poiName.text = selectedTarget.event.name;
	}
	
	
}


- (void)viewDidUnload {
	headerView = nil;
	poiName = nil;
	[super viewDidUnload];
}
@end

@implementation ARtableCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleGray;
		self.textLabel.backgroundColor = [UIColor clearColor];
		self.textLabel.textColor = [UIColor blackColor];
		self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
		self.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
	}
	return self;
	
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	self.contentView.backgroundColor = [UIColor clearColor];
	self.backgroundColor = [UIColor clearColor];
	CGRect textLabelFrame = self.textLabel.frame;
	textLabelFrame.size.width = 180;
	self.textLabel.frame = textLabelFrame;
	CGRect detailLabelFrame = self.detailTextLabel.frame;
	detailLabelFrame.size.width = 70;
	detailLabelFrame.origin.x = 190;
	self.detailTextLabel.frame = detailLabelFrame;
}

@end

@implementation MapViewAnnotation

@synthesize title, coordinate;

-(id)initWithTitle:(NSString *)ttl andcood:(CLLocationCoordinate2D)c2d {
	self = [super init];
	title = ttl;
	coordinate = c2d;
	return self;
}
@end
