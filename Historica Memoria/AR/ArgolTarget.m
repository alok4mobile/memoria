//
//  ARLocationTarget.m
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ArgolTarget.h"
#import "ArgolDevice.h"
#import "ArgolView.h"

@interface ArgolTarget()

@property (nonatomic) Vector3D targetSize;

@end

@implementation ArgolTarget

@synthesize device;
@synthesize location;
@synthesize azimuth;
@synthesize distance;
@synthesize description;
@synthesize position, visible, tag, phi, theta, targetSize;

- (id) init {
    self = [super init];
    
    self.visible = NO;
    self.tag = 0;
    
    self.phi = [[NumberSmoother alloc] initWithFloat:0.0 smoothingFactor:1];
    self.theta = [[NumberSmoother alloc] initWithFloat:0.0 smoothingFactor:1];
	
    return self;
}

- (id) initWithDevice:(ArgolDevice *)aDevice targetLocation:(CLLocation*)tarLocation {
    self = [self init];
    
    self.device = aDevice;
    self.location = tarLocation;
    
    self.description = [NSString stringWithFormat:@"Target: %f N, %f E", self.location.coordinate.latitude, self.location.coordinate.longitude];
    
    NSInteger defaultFactor = POSITION_SMOOTHING_FACTOR_DEFAULT;
    [self updateSmoothingFactor:defaultFactor];
    
    return self;
}


- (void) dealloc {
	// [super dealloc];
}

- (void) check {
    if (self.location == nil)
        return;
    [self checkDistance];
    [self checkAzimuth];
    [self checkViewField];
	
    //[self performSelectorOnMainThread:@selector(display) withObject:self waitUntilDone:NO];
	
	[self display];
}

- (BOOL) checkDistance {
    BOOL reached = NO;
    
    self.distance = [self.location distanceFromLocation:self.device.currLocation];
    
    //NSLog(@"distance: %f; azimuth: %f", self.distance, self.azimuth);
    
    if (self.distance < AR_TARGET_REACHED_DISTANCE)
    {
        reached = YES;
    }
    
    return reached;
}

- (void) checkAzimuth {
    self.azimuth = [ArgolDevice getHeadingForDirectionFromCoordinate:self.device.currLocation.coordinate toCoordinate:self.location.coordinate];
}

- (double) getVerticalDistance {
    double deviceAlt = [self.device getAltitude];
    double targetAlt = self.location.altitude;
    
    return targetAlt - deviceAlt;
}

- (BOOL) checkViewField {
    
    double targetX = self.distance * sin([MathUtils degreesToRadians:self.azimuth]);
    double targetY = self.distance * cos([MathUtils degreesToRadians:self.azimuth]);
    double targetZ = [self getVerticalDistance];
    
    self.position = [MathUtils vector3DMakeX:targetX y:targetY z:targetZ];
	
    self.position = [MathUtils applyTransform:self.device.rotZ toVector:self.position];
    self.position = [MathUtils applyTransform:self.device.rotY toVector:self.position];
    self.position = [MathUtils applyTransform:self.device.rotX toVector:self.position];
    
    double newTheta = atan(self.position.x/self.position.z);
    double newPhi = atan(self.position.y/self.position.z);
	
    [self.theta smooth:newTheta];
    [self.phi smooth:newPhi];
    
    if (ABS([self.theta last]) < self.device.maxTheta && ABS([self.phi last]) < self.device.maxPhi && self.position.z < 0)
    {
        self.visible = YES;
    }
    else
    {
        self.visible = NO;
    }
    return self.visible;
}

- (CGPoint) getProjection {
    CGPoint projection = CGPointMake(-1, -1);
    if (!self.visible)
        return projection;
    projection.x = -[self.theta last]/self.device.maxTheta;
    projection.y = -[self.phi last]/self.device.maxPhi;
    
    return projection;
}

- (void) display {
    
}

- (void) updateSmoothingFactor:(NSInteger)newFactor {
    [self.phi updateSmoothingFactor:newFactor];
    [self.theta updateSmoothingFactor:newFactor];
}

- (CGSize) getSize {
    
    return CGSizeMake(self.targetSize.x, self.targetSize.y);
}

- (NSComparisonResult) compareDistance:(ArgolTarget *)targetToCompare {
    if (self.distance < targetToCompare.distance) {
        return NSOrderedAscending;
    } else if (self.distance > targetToCompare.distance) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

@end
