//
//  LocationManager.h
//  LocationTest
//
//  Created by Aleš Podolník on 21/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
#import <QuartzCore/QuartzCore.h>
#import "MathUtils.h"
#import "NumberSmoother.h"

#define GRAVITY_SMOOTHING_FACTOR_DEFAULT 5;
#define POSITION_SMOOTHING_FACTOR_DEFAULT 5;

@class ArgolTarget;

@interface ArgolDevice : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager* coreLocMgr;

@property (nonatomic, strong) CLHeading* prevHeading;
@property (nonatomic, strong) CLHeading* currHeading;

@property (nonatomic, strong) CLLocation* prevLocation;
@property (nonatomic, strong) CLLocation* currLocation;

@property (nonatomic, strong) CMMotionManager* motionManager;
@property (nonatomic, strong) CMAttitude *attitude;
@property (nonatomic) CMAcceleration acceleration;

@property (nonatomic) double maxTheta;
@property (nonatomic) double maxPhi;

@property (nonatomic, strong) NSMutableArray* targets;
@property (nonatomic) CMMagneticField magneticField;
@property (nonatomic) double magneticFieldDirection;
@property (nonatomic) double northPoleDirection;

@property (nonatomic) CATransform3D rotX;
@property (nonatomic) CATransform3D rotY;
@property (nonatomic) CATransform3D rotZ;
@property (nonatomic) double alpha;
@property (nonatomic) double beta;
@property (nonatomic) double gamma;
@property (nonatomic) double zeroYaw;
@property (nonatomic) Vector3D gravity;
@property (nonatomic) Vector3D magnetic;
@property (nonatomic) NSTimeInterval checkInterval;
@property (nonatomic) BOOL checkingTargets;

@property (nonatomic, strong) NumberSmoother* gravitySmootherX;
@property (nonatomic, strong) NumberSmoother* gravitySmootherY;
@property (nonatomic, strong) NumberSmoother* gravitySmootherZ;

- (id) init;
- (void) dealloc;

- (void) startCheckingLocation;
- (void) startCheckingTargets;
- (void) stopCheckingTargets;

- (void) repeatChecking;
- (void) checkTargets;
- (void) motionUpdated:(CMDeviceMotion*)motion;
- (void) accelerometerUpdated:(CMAccelerometerData*)data;

+ (float) getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc;

- (double) getAltitude;
- (Vector3D) getGravityMean:(Vector3D)actual;

- (void) updateZeroYaw;
- (double) getAdjustedGamma;

- (void) updateGravitySmoothingFactor:(NSInteger)newFactor;

- (ArgolTarget*)getTarget:(NSInteger)tag;

@end
