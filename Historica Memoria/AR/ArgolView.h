//
//  ARView.h
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class ArgolTarget;

@interface ArgolView : UIView;

@property (nonatomic) CGRect cameraFrame;

- (id) initWithFrame:(CGRect)frame;
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position;

- (CGPoint)getCenterFromCoordinates:(CGPoint)coords;

- (void) loadTargets;
- (void) updated:(ArgolTarget*)target;
@end
