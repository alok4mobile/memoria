//
//  LocationManager.m
//  LocationTest
//
//  Created by Aleš Podolník on 21/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ArgolDevice.h"
#import "PhoneCamera.h"
#import "ArgolTarget.h"

const CLLocationCoordinate2D NORTH_POLE = {.latitude = 82.7, .longitude =-114.4};
const CGFloat MAXIIMAL_DISTANCE_TO_SHOW = 5000; // metres

@interface ArgolDevice()

@property (nonatomic) NSInteger targetCounter;
@property (nonatomic, strong) NSThread *targetChecker;
@property (nonatomic) BOOL firstCheck;
@property (nonatomic) BOOL motionRecieved;
@property (nonatomic) BOOL locationRecieved;

@end

@implementation ArgolDevice

@synthesize coreLocMgr = _coreLocMgr;
@synthesize prevHeading = _prevHeading;
@synthesize prevLocation = _prevLocation;
@synthesize currHeading = _currHeading;
@synthesize currLocation = _currLocation;
@synthesize targets = _targets;
@synthesize motionManager;
@synthesize attitude;
@synthesize acceleration;
@synthesize maxPhi, maxTheta, rotX, rotY, rotZ, gravity, alpha, beta, gamma, magneticField, magneticFieldDirection, zeroYaw, checkInterval, checkingTargets, targetChecker, gravitySmootherX, gravitySmootherY, gravitySmootherZ, magnetic, northPoleDirection, firstCheck;

@synthesize motionRecieved, locationRecieved;

@synthesize targetCounter;

- (id) init {
    self = [super init];
    
    self.targetCounter = 0;
    self.zeroYaw = 0;
    
    self.targets = [[NSMutableArray alloc] init];
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.deviceMotionUpdateInterval = 0.1;
    self.motionManager.accelerometerUpdateInterval = 0.01;
    
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:0.01];
    
    self.gravity = [MathUtils vector3DMakeX:0 y:0 z:0];
    self.magnetic = [MathUtils vector3DMakeX:0 y:0 z:0];
	
    self.maxTheta = [PhoneCamera horizontalViewHalfAngle];
    self.maxPhi = [PhoneCamera verticalViewHalfAngle];
    
    self.checkingTargets = NO;
    self.checkInterval = 0.05;
    
    self.targetChecker = nil;
    
    NSInteger defaultFactor = GRAVITY_SMOOTHING_FACTOR_DEFAULT;
    self.gravitySmootherX = [[NumberSmoother alloc] initWithFloat:0.0 smoothingFactor:defaultFactor];
    self.gravitySmootherY = [[NumberSmoother alloc] initWithFloat:0.0 smoothingFactor:defaultFactor];
    self.gravitySmootherZ = [[NumberSmoother alloc] initWithFloat:0.0 smoothingFactor:defaultFactor];
    
    self.firstCheck = YES;
    self.northPoleDirection = 0;
    
    return self;
}

- (void) dealloc {
    
	
}

- (void) startCheckingLocation {
    
    self.coreLocMgr = [[CLLocationManager alloc] init] ;
    self.coreLocMgr.delegate = self;
    
    [self.coreLocMgr startUpdatingHeading];
    [self.coreLocMgr startUpdatingLocation];
    
    if ([self.motionManager isDeviceMotionAvailable]) {
        id motionHandler = ^ (CMDeviceMotion *motion, NSError *error) {
            [self motionUpdated:motion];
        };
        
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXTrueNorthZVertical toQueue:[NSOperationQueue currentQueue]  withHandler:motionHandler];
    }
    
    if ([self.motionManager isAccelerometerAvailable]) {
        id accelerometerHandler = ^ (CMAccelerometerData *motion, NSError *error) {
            [self accelerometerUpdated:motion];
        };
        
        [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:accelerometerHandler];
    }
    
    self.motionRecieved = NO;
    self.locationRecieved = NO;
    
}

- (void) motionUpdated:(CMDeviceMotion *)motion {
    self.attitude = motion.attitude;
    //NSLog(@"roll: %f, pitch: %f, yaw: %f", self.attitude.roll, self.attitude.pitch, self.attitude.yaw);
    
    if (ABS(self.attitude.yaw) < [MathUtils degreesToRadians:1]) {
        [self updateZeroYaw];
    }
	
    double magX = motion.magneticField.field.x;
    double magY = motion.magneticField.field.y;
    double magZ = motion.magneticField.field.z;
    
    self.magneticField = motion.magneticField.field;
    
    double mag = sqrt(magX*magX + magY*magY + magZ*magZ);
    magX = magX/mag;
    magY = magY/mag;
    magZ = magZ/mag;
    
	
    //NSLog(@"magX: %f, magY: %f, magZ: %f", magX, magY, magZ);
    
    
    self.magnetic = [MathUtils vector3DMakeX:magX y:magY z:magZ];
    
    self.magnetic = [MathUtils applyTransform:[MathUtils rotY:-ABS(self.beta)] toVector:self.magnetic];
    self.magnetic = [MathUtils applyTransform:[MathUtils rotX:-ABS(self.alpha)] toVector:self.magnetic];
    
    self.magneticFieldDirection = -atan(self.magnetic.x/self.magnetic.y);
	
    if (magX <= 0 && magY >= 0) {
        
    } else if (self.magnetic.x <= 0 && self.magnetic.y <= 0) {
        self.magneticFieldDirection = M_PI + self.magneticFieldDirection;
    } else if (self.magnetic.x >= 0 && self.magnetic.y <= 0) {
        self.magneticFieldDirection = M_PI + self.magneticFieldDirection;
    } else if (self.magnetic.x >= 0 && self.magnetic.y >= 0) {
        self.magneticFieldDirection = M_PI*2 + self.magneticFieldDirection;
    }
    
    self.magneticFieldDirection -= [MathUtils degreesToRadians:self.northPoleDirection];
    
	
	
    //NSLog(@"Motion updated");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MotionUpdated" object:self];
    
    self.motionRecieved = YES;
}

- (void) accelerometerUpdated:(CMAccelerometerData *)data {
    self.acceleration = data.acceleration;
    //NSLog(@"x: %f, y: %f, z: %f", self.acceleration.x, self.acceleration.y, self.acceleration.z);
    
    //NSLog(@"Accelerometer updated");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccelerometerUpdated" object:self];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    self.prevHeading = self.currHeading;
    self.currHeading = newHeading;
    
    if (self.firstCheck) {
        self.firstCheck = NO;
        self.northPoleDirection = newHeading.magneticHeading - newHeading.trueHeading;
    }  //  [self checkTargets];
    
	//NSLog(@"heading: %f",self.currHeading.trueHeading);
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"HeadingUpdated" object:self];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    self.prevLocation = oldLocation;
    self.currLocation = newLocation;
	
	//  [self checkTargets];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationUpdated" object:self];
	
    self.locationRecieved = YES;
	
}

- (void) checkTargets {
    
    Vector3D actualGravity = [MathUtils vector3DMakeX:self.acceleration.x y:self.acceleration.y z:self.acceleration.z];
    
    actualGravity = [self getGravityMean:actualGravity];
    
    double gx = actualGravity.x;
    double gy = actualGravity.y;
    double gz = actualGravity.z;
    //NSLog(@"Gravity measured: %f, %f, %f", gx, gy, gz);
    
    double maxG = MAX(MAX(ABS(gx), ABS(gy)), ABS(gz));
    gx = floor(100 * gx/maxG) / 100;
    gy = floor(100 * gy/maxG) / 100;
    gz = floor(100 * gz/maxG) / 100;
    double g = sqrt(gx*gx + gy*gy + gz*gz);
    
    //NSLog(@"Gravity adjusted: %f, %f, %f", gx, gy, gz);
    
    if (gx == g)
        self.alpha = gy > 0 ? -M_PI_2 : M_PI_2;
    else if (- gy/sqrt(g*g - gx*gx) >= 1)
        self.alpha = M_PI_2;
    else
        self.alpha = gz < 0 ? asin(- gy/sqrt(g*g - gx*gx)) : M_PI - asin(- gy/sqrt(g*g - gx*gx));
    
    //NSLog(@"sin: %f; asin: %f; gx: %f; g: %f", - gy/sqrt(g*g - gx*gx), asin(- gy/sqrt(g*g - gx*gx)), gx, g);
    
    self.beta = asin(gx/g);
    //NSLog(@"gamma - %f", self.currHeading.trueHeading);
    
    self.gamma = [self getAdjustedGamma];
    //NSLog(@"Device orientation: %f, %f, %f", [GeometryUtils radiansToDegrees:self.alpha], [GeometryUtils radiansToDegrees:self.beta], [GeometryUtils radiansToDegrees:self.gamma]);
    
    
    self.rotX =  CATransform3DMakeRotation(-self.alpha, 1, 0, 0);
    self.rotY =  CATransform3DMakeRotation(-self.beta, 0, 1, 0);
    self.rotZ =  CATransform3DMakeRotation(-self.gamma, 0, 0, 1);
	
    for (ArgolTarget *target in self.targets) {
		[target check];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"4mAR_TARGETS_CHECKED" object:nil];
}

+ (float) getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    //NSLog(@"From: %f N, %f E to %f N, %f E", fromLoc.latitude, fromLoc.longitude, toLoc.latitude, toLoc.longitude);
    
    float lat1 = [MathUtils degreesToRadians:fromLoc.latitude];
    float lon1 = [MathUtils degreesToRadians:fromLoc.longitude];
    float lat2 = [MathUtils degreesToRadians:toLoc.latitude];
    float lon2 = [MathUtils degreesToRadians:toLoc.longitude];
    
    float bearing = atan2(cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon2-lon1), sin(lon2-lon1)*cos(lat2));
    bearing = [MathUtils radiansToDegrees:bearing];
    bearing = - bearing + 90;
    if (bearing < 0)
        bearing += 360;
    
    //NSLog(@"Bearing: %f", bearing);
    return bearing;
}

- (double) getAltitude {
    return self.currLocation.altitude;
}

- (Vector3D) getGravityMean:(Vector3D)actual {
    
    if (self.gravity.x == 0 && self.gravity.y == 0 && self.gravity.z == 0){
        self.gravity = actual;
        return actual;
    }
    else {
        
        double x =[self.gravitySmootherX smooth:actual.x];
        // [MathUtils smooth:actual.x with:self.gravity.x last:n];//(4*self.gravity.x + actual.x)/5;
        double y =[self.gravitySmootherY smooth:actual.y];
        // [MathUtils smooth:actual.y with:self.gravity.y last:n];//(4*self.gravity.y + actual.y)/5;
        double z =[self.gravitySmootherZ smooth:actual.z];
        // [MathUtils smooth:actual.z with:self.gravity.z last:n];//(4*self.gravity.z + actual.z)/5;
        
        self.gravity = [MathUtils vector3DMakeX:x y:y z:z];
        
        return self.gravity;
    }
}

- (void) updateZeroYaw {
    self.zeroYaw = [MathUtils degreesToRadians:self.currHeading.trueHeading];
}

- (double) getAdjustedGamma {
    //return M_PI - self.magneticFieldDirection;
    return [MathUtils degreesToRadians:180-self.currHeading.trueHeading] + self.beta;
    //return M_PI - (self.zeroYaw - self.attitude.yaw);
}

- (void) startCheckingTargets {
    self.checkingTargets = YES;
    
    self.targetChecker = [[NSThread alloc] initWithTarget:self selector:@selector(repeatChecking) object:nil];
    [self.targetChecker start];
    //[NSThread detachNewThreadSelector:@selector(repeatChecking) toTarget:self withObject:nil];
}

- (void) stopCheckingTargets {
    [self.targetChecker cancel];
    self.targetChecker = nil;
    self.checkingTargets = NO;
    [NSThread sleepForTimeInterval:2*self.checkInterval];
}

- (void) repeatChecking {
    //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    [NSThread sleepForTimeInterval:3*self.checkInterval];
    while (self.checkingTargets) {
        if (self.locationRecieved) {
            [self checkTargets];
            // NSLog(@"Targets checked");
        }
        [NSThread sleepForTimeInterval:self.checkInterval];
    }
    //[pool release];
}

- (void) updateGravitySmoothingFactor:(NSInteger)newFactor {
    [self.gravitySmootherX updateSmoothingFactor:newFactor];
    [self.gravitySmootherY updateSmoothingFactor:newFactor];
    [self.gravitySmootherZ updateSmoothingFactor:newFactor];
}

- (ArgolTarget*) getTarget:(NSInteger)tag {
    for (ArgolTarget *target in self.targets) {
        if (target.tag == tag)
            return target;
    }
    return nil;
}


@end
