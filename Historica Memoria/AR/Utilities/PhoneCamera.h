//
//  PhoneCamera.h
//  LocationTest
//
//  Created by Aleš Podolník on 19/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhoneCamera : NSObject

/*
 Az to bude hotovy, tak by to melo pro kazdej druh telefonu vratit spravny rozmery vztahujici se ke kamere.
 */

+ (double) focalLength;
+ (double) horizontalViewHalfAngle;
+ (double) verticalViewHalfAngle;
+ (double) horizontalViewAngle;
+ (double) verticalViewAngle;
+ (double) sensorWidth;
+ (double) sensorHeight;

@end
