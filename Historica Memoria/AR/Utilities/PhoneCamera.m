//
//  PhoneCamera.m
//  LocationTest
//
//  Created by Aleš Podolník on 19/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhoneCamera.h"

@implementation PhoneCamera

+ (double) focalLength {
    return 3.85;
}

+ (double) horizontalViewHalfAngle {
    return atan(([PhoneCamera sensorWidth]/2)/[PhoneCamera focalLength]);
}

+ (double) verticalViewHalfAngle {
    return atan(([PhoneCamera sensorHeight]/2)/[PhoneCamera focalLength]);
}

+ (double) horizontalViewAngle {
    return 2*[PhoneCamera horizontalViewHalfAngle];
}

+ (double) verticalViewAngle {
    return 2*[PhoneCamera verticalViewHalfAngle];
}

+ (double) sensorWidth {
    return 4.52;
}

+ (double) sensorHeight {
    return 3.39;
}

@end
