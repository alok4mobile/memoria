//
//  ConversionUtils.m
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MathUtils.h"

@implementation MathUtils

+ (CGFloat) degreesToRadians:(CGFloat)degrees {
    return degrees * M_PI / 180;
}

+ (CGFloat) radiansToDegrees:(CGFloat)radians {
    return radians * 180 / M_PI;
}

+ (Vector3D) applyTransform:(CATransform3D)matrix toVector:(Vector3D)vector {
    Vector3D result;
    result.x = 1.0*matrix.m11*vector.x + 1.0*matrix.m12*vector.y + 1.0*matrix.m13*vector.z + 1.0*matrix.m14*vector.d;
    result.y = 1.0*matrix.m21*vector.x + 1.0*matrix.m22*vector.y + 1.0*matrix.m23*vector.z + 1.0*matrix.m24*vector.d;
    result.z = 1.0*matrix.m31*vector.x + 1.0*matrix.m32*vector.y + 1.0*matrix.m33*vector.z + 1.0*matrix.m34*vector.d;
    result.d = 1.0*matrix.m41*vector.x + 1.0*matrix.m42*vector.y + 1.0*matrix.m43*vector.z + 1.0*matrix.m44*vector.d;
    
    return result;
}

+ (Vector3D) vector3DMakeX:(double)v1 y:(double)v2 z:(double)v3 {
    Vector3D vector;
    vector.x = v1;
    vector.y = v2;
    vector.z = v3;
    vector.d = 0;
    
    return vector;
}

+ (CATransform3D) rotX:(CGFloat)angle {
    return CATransform3DMakeRotation(angle, 1, 0, 0);
}

+ (CATransform3D) rotY:(CGFloat)angle {
    return CATransform3DMakeRotation(angle, 0, 1, 0);
}

+ (CATransform3D) rotZ:(CGFloat)angle {
    return CATransform3DMakeRotation(angle, 0, 0, 1);
    
}
/*
+ (CGFloat)smooth:(CGFloat)newData with:(CGFloat)old last:(NSInteger)n {
    return ((n - 1)*old + newData)/n;
}
*/
@end
