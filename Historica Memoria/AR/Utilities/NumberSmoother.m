//
//  NumberSmoother.m
//  LocationTest
//
//  Created by Aleš Podolník on 19/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NumberSmoother.h"

@implementation NumberSmoother

@synthesize values, smoothingFactor;

- (id) initWithNumber:(NSNumber*)number smoothingFactor:(NSInteger)factor {
    self = [super init];
    self.values = [[NSMutableArray alloc] init] ;
    [self.values addObject:number];
    self.smoothingFactor = 1;
    [self updateSmoothingFactor:factor];   
        
    return self;
}

- (id) initWithFloat:(CGFloat)number smoothingFactor:(NSInteger)factor {
    NSNumber *numberObject = [NSNumber numberWithFloat:number];
    return [self initWithNumber:numberObject smoothingFactor:factor];
}

- (void) dealloc {
	
}

- (void) updateSmoothingFactor:(NSInteger)factor {
    if (factor < 1){
        NSLog(@"It is illegal to have smoothing factor < 1");
        return;
    }
    
    if (factor > self.smoothingFactor) {
        NSNumber *seed;
        
        if (self.values.count > 0) {
            seed = [self.values objectAtIndex:self.values.count - 1];
        } else {
            seed = [NSNumber numberWithFloat:0.0];
        }
        
        while (self.values.count < factor) {
            [self.values addObject:[seed copy]];
        }
        
    } else if (factor < self.smoothingFactor) {
        if (self.values.count > factor) {
            [self.values removeObjectAtIndex:0];
        }
    } else {
        if (factor == 1 && self.values.count == 0) {
            NSNumber *seed = [NSNumber numberWithFloat:0.0];
            [self.values addObject:seed];
        }
    }
    
    self.smoothingFactor = factor;
}

- (NSNumber*) smoothNative:(NSNumber*)number {
    if (self.values.count > 0) {
        [self.values removeObjectAtIndex:0];
        [self.values addObject:number];
        
        CGFloat sum = 0;
        for (NSNumber *n in self.values) {
            sum += [n floatValue];
        }
        return [NSNumber numberWithFloat:1.0*sum/self.values.count];        
    } else {
        return [NSNumber numberWithFloat:0.0];
    }
}

- (CGFloat) smooth:(CGFloat)number {
    return [[self smoothNative:[NSNumber numberWithFloat:number]] floatValue];
}

- (NSNumber*) lastNative {
    if (self.values.count > 0)
        return [self.values objectAtIndex:self.values.count - 1];
    
    return [NSNumber numberWithFloat:0.0];
}
- (CGFloat) last {
    return [[self lastNative] floatValue];
}

@end
