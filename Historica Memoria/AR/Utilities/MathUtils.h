//
//  ConversionUtils.h
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef struct 
{ 
    CGFloat x, y, z, d;
} Vector3D;

@interface MathUtils : NSObject

+ (CGFloat) degreesToRadians:(CGFloat)degrees;
+ (CGFloat) radiansToDegrees:(CGFloat)radians;
+ (Vector3D) applyTransform:(CATransform3D)matrix toVector:(Vector3D)vector;
+ (Vector3D) vector3DMakeX:(double)v1 y:(double)v2 z:(double)v3;

+ (CATransform3D) rotX:(CGFloat)angle;
+ (CATransform3D) rotY:(CGFloat)angle;
+ (CATransform3D) rotZ:(CGFloat)angle;

//+ (CGFloat)smooth:(CGFloat)newData with:(CGFloat)old last:(NSInteger)n;
@end
