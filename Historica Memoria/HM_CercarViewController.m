//
//  HM_CercarViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 10/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_CercarViewController.h"

#import "HM_AppDelegate.h"

#import "HM_SearchResultsViewController.h"

@class searchCell;

@interface HM_CercarViewController () {
	searchCell *_seachcell;
	
}
@end

@interface searchCell : UITableViewCell

@end

@implementation HM_CercarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
        self.title = NSLocalizedString(@"Buscar",nil);
		self.tabBarItem.image = [UIImage imageNamed:@"search_unactive"];
		self.tabBarController.navigationItem.leftBarButtonItem = nil;
	}
    return self;
}

- (void)viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"05-background"]];
	self.tabBarController.navigationItem.leftBarButtonItem = nil;
	// Our table view that contains search boxes
	[_tableSearchPage removeFromSuperview];
	[_hiddenTable removeFromSuperview];
	
	_tableSearchPage = [[UITableView alloc] initWithFrame:CGRectMake(10, 30, self.view.frame.size.width - 22, 300) style:UITableViewStylePlain];
	_tableSearchPage.delegate = self;
	_tableSearchPage.dataSource = self;
	_tableSearchPage.backgroundColor = [UIColor clearColor];
	_tableSearchPage.scrollEnabled = NO;
	_tableSearchPage.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
	_tableSearchPage.separatorColor = [UIColor clearColor];
	_tableSearchPage.rowHeight = 25;
	[_tableSearchPage reloadData];
	
	//hidden tableView that contains the elements that we want to search
	
	_hiddenTable = [[UITableView alloc] initWithFrame:CGRectMake(10, hiddentableOffset, self.view.frame.size.width - 22, 140) style:UITableViewStylePlain];
	_hiddenTable.delegate = self;
	_hiddenTable.dataSource = self;
	_hiddenTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
	_hiddenTable.rowHeight = 30;
	_hiddenTable.hidden = YES;
	
	// Drop down buttons
	
	CGFloat buttonOffset = 10;
	CGFloat buttonWidth = self.view.frame.size.width - 20;
	
	UIImageView *dropDownButton = [[UIImageView alloc] initWithFrame:CGRectMake(buttonWidth - 25, 0, 25, 25)];
	dropDownButton.image = [UIImage imageNamed:@"05-display button_OFF"];
	
	UIButton *firstButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonOffset, 31, buttonWidth, 25)];
	[firstButton addSubview:dropDownButton];
	[firstButton addTarget:self action:@selector(eventPressed:) forControlEvents:UIControlEventTouchUpInside];
	
	dropDownButton = [[UIImageView alloc] initWithFrame:CGRectMake(buttonWidth - 25, 0, 25, 25)];
	dropDownButton.image = [UIImage imageNamed:@"05-display button_OFF"];
	
	UIButton *secondButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonOffset, firstButton.frame.origin.y + 80, buttonWidth, 25)];
	[secondButton addSubview:dropDownButton];
	[secondButton addTarget:self action:@selector(eventWithRoutePressed:) forControlEvents:UIControlEventTouchUpInside];
	
	dropDownButton = [[UIImageView alloc] initWithFrame:CGRectMake(buttonWidth - 25, 0, 25, 25)];
	dropDownButton.image = [UIImage imageNamed:@"05-display button_OFF"];
	
	
	UIButton *thirdButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonOffset, secondButton.frame.origin.y + 80, buttonWidth, 25)];
	[thirdButton addSubview:dropDownButton];
	[thirdButton addTarget:self action:@selector(namesWithEventPressed:) forControlEvents:UIControlEventTouchUpInside];
	
	// accept and clear buttons
	
	CGFloat bottomButtonyOffset = 280;
	CGFloat bottomButtonxOffset = 40;
	
	UIButton *acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(bottomButtonxOffset, bottomButtonyOffset, 100, 30)];
	[acceptButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
	[acceptButton addTarget:self action:@selector(acceptButtonPressed) forControlEvents:UIControlEventTouchUpInside];
	
	UILabel *acceptlabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 90, 20)];
	acceptlabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
	acceptlabel.backgroundColor = [UIColor clearColor];
	acceptlabel.textAlignment = UITextAlignmentCenter;
	acceptlabel.textColor = [UIColor colorWithRed:56.0f/255.0f green:62.0f/255.0f blue:64.0f/255.0f alpha:1.0];
	
	UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake(bottomButtonxOffset + 140, bottomButtonyOffset, 100, 30)];
	[clearButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
	[clearButton addTarget:self action:@selector(clearButtonPressed) forControlEvents:UIControlEventTouchUpInside];
	
	UILabel *clearLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 90, 20)];
	clearLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
	clearLabel.backgroundColor = [UIColor clearColor];
	clearLabel.textAlignment = UITextAlignmentCenter;
	clearLabel.textColor = [UIColor colorWithRed:56.0f/255.0f green:62.0f/255.0f blue:64.0f/255.0f alpha:1.0];
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	if ([language isEqualToString:@"ca"])
	{
		acceptlabel.text = @"ACCEPTAR";
		clearLabel.text = @"NETEJAR";
	}
	else if (![language isEqualToString:@"ca"])
	{
		acceptlabel.text =@"ACEPTAR";
		clearLabel.text = @"LIMPIAR";
	}
	
	
	[acceptButton addSubview:acceptlabel];
	[clearButton addSubview:clearLabel];
	
	[self.view addSubview:_tableSearchPage];
	[self.view addSubview:firstButton];
	[self.view addSubview:secondButton];
	[self.view addSubview:thirdButton];
	[self.view addSubview:acceptButton];
	[self.view addSubview:clearButton];
	[self.view addSubview:_hiddenTable];
	
}

#pragma mark accept and clear button action

-(void)acceptButtonPressed
{
//	[[HM_AppDelegate sharedAppDelegate] hideTitle];
	NSArray *_newData = [[[HM_AppDelegate sharedAppDelegate].data  searchResults:_chronological:_district:_localization] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	HM_SearchResultsViewController *nextView = [[HM_SearchResultsViewController alloc] initWithData:_newData];
	NSLog(@"Search Data <%@> with count <%i>",_newData,_newData.count);
	[self.navigationController pushViewController:nextView animated:YES];
	
}

-(void)clearButtonPressed {
	_hiddenTable.hidden = YES;
	_district = nil;
	_chronological = nil;
	_localization = nil;
	[_tableSearchPage reloadData];
	
}

#pragma mark drop down menu button pressed

- (void)setSituationalCase:(NSString *)rowSelected
{
	switch (caseKey) {
		case(0):
			_district = rowSelected;
			[_tableSearchPage reloadData];
			
			break;
		case(1):
			_chronological = rowSelected;
			[_tableSearchPage reloadData];
			
			break;
		case(2):
			_localization = rowSelected;
			[_tableSearchPage reloadData];
			
			break;
	}
}

-(void)eventPressed:(id)sender
{
	caseKey = 0;
	_hiddenTable.hidden = YES;
	hiddentableOffset = 55;
	CGRect frame = _hiddenTable.frame;
	frame.origin.y = hiddentableOffset;
	_hiddenTable.frame = frame;
	_hiddenTable.hidden = NO;
	searchData = nil;
	searchData = [[[[HM_AppDelegate sharedAppDelegate].data RouteWithEvent:_chronological]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
	[_hiddenTable reloadData];
}

-(void)eventWithRoutePressed:(id)sender
{
	caseKey = 1;
	
	_hiddenTable.hidden = YES;
	hiddentableOffset = 135;
	CGRect frame = _hiddenTable.frame;
	frame.origin.y = hiddentableOffset;
	_hiddenTable.frame = frame;
	_hiddenTable.hidden = NO;
	searchData = nil;
	searchData = [[[[HM_AppDelegate sharedAppDelegate].data eventWithRoute:_district] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
	[_hiddenTable reloadData];
}

-(void)namesWithEventPressed:(id)sender
{
	caseKey = 2;
	
	_hiddenTable.hidden = YES;
	hiddentableOffset = 215;
	CGRect frame = _hiddenTable.frame;
	frame.origin.y = hiddentableOffset;
	_hiddenTable.frame = frame;
	_hiddenTable.hidden = NO;
	searchData = nil;
	searchData = [[[[HM_AppDelegate sharedAppDelegate].data namesWithEventandRoute:_chronological:_district] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
	[_hiddenTable reloadData];
}

#pragma mark Settings for TableView header and footer

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (tableView == _tableSearchPage) {
		NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
		if (sectionTitle == nil) {
			return nil;
		}
		UIView *sectionHeader = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.origin.x - 2, 0, tableView.bounds.size.width, 25)];
		
		sectionHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar new"]];
		
		UILabel *headerView = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, sectionHeader.frame.size.width - 10, sectionHeader.frame.size.height)];
		headerView.backgroundColor = [UIColor clearColor];
		headerView.textColor = [UIColor whiteColor];
		headerView.text = sectionTitle;
		headerView.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
		[sectionHeader addSubview:headerView];
		return sectionHeader;
	}
	
	else if (tableView == _hiddenTable) {
		return Nil;
	}
	return Nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	
	if (tableView == _tableSearchPage) {
		UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
		footerView.backgroundColor = [UIColor clearColor];
		return footerView;
		
	}
	return Nil;
}


#pragma mark settings for TableView sections

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (tableView == _tableSearchPage) {
		return 3;
	}
	else {
		return 1;
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (tableView == _tableSearchPage) {
		return 1;
	}
	else if (tableView == _hiddenTable) {
		return searchData.count;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *CellIdentifier = [ NSString stringWithFormat: @"%d:%d", [ indexPath indexAtPosition: 0 ], [ indexPath indexAtPosition:1 ]];
	
	_seachcell = nil;
	if (tableView == _tableSearchPage) {
		_seachcell = [ tableView dequeueReusableCellWithIdentifier: CellIdentifier];
		_seachcell = nil;
		
		if (_seachcell == nil) {
			_seachcell = [[ searchCell alloc ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
			_seachcell.textLabel.textColor = [UIColor whiteColor];
			_seachcell.selectionStyle = UITableViewCellSelectionStyleNone;
			_seachcell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
			_seachcell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
			_seachcell.textLabel.backgroundColor = [UIColor clearColor];
			switch ([indexPath section]) {
				case(0):
					
					switch([indexPath row]) {
							
						case(0):
						{
							_seachcell.textLabel.text = _district; // OS3
							NSLog(@"_district = <%@>",_district);
						}
							break;
							
					}
					
					break;
					
				case(1):
					
					switch ([ indexPath indexAtPosition: 1 ]) {
							
						case(0):
						{
							_seachcell.textLabel.text = _chronological; // OS3
							NSLog(@"_chronological = <%@>",_chronological);
						}
							break;
					}
					break;
				case(2):
					
					switch ([ indexPath indexAtPosition: 1 ]) {
							
						case(0):
						{
							_seachcell.textLabel.text = _localization; // OS3
							NSLog(@"_localization = <%@>",_localization);
						}
							break;
					}
					break;
			}
		}
		return _seachcell;
	}
	
	else if (tableView == _hiddenTable)
	{
		NSString *cellIdentifier = @"Cell Identifier";
		//[self loadData];
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		
		cell = nil;
		if (cell==nil) {
			
			cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
			cell.textLabel.text = [searchData objectAtIndex:indexPath.row];
			cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
			cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
			cell.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
		}
		return cell;
		
	}
	return Nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView == _hiddenTable) {
		
		_tempString = [searchData objectAtIndex:indexPath.row];
		_hiddenTable.hidden = YES;
		NSLog(@"selected:<%@>",_tempString);
		[self setSituationalCase:_tempString];
	}
	
}

#pragma mark locatlized title



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	NSString *barrios;
	NSString *chrono;
	NSString *local;
	
	
	if ([language isEqualToString:@"ca"])
	{	barrios = @"BARRIS";
		chrono = @"CRONOLOGIA";
		local = @"LOCALIZACIONS";
	}
	else if (![language isEqualToString:@"ca"])
	{	barrios = @"BARRIOS";
		chrono = @"CRONOLOGÍA";
		local = @"LOCALIZACIONES";
	}
    if (tableView == _tableSearchPage) {
		switch (section) {
				
			case(0):
				return barrios;
				break;
			case(1):
				return chrono;
                break;
			case(2):
				return local;
                break;
		}
    }
	
	return nil;
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	
	if (tableView == _tableSearchPage)
	{
		return 25.0f;
	}
	else {
		return 0;
	}
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	
	if (tableView == _tableSearchPage)
	{
		return 30.0f;
	}
	else {
		return 0;
	}
}


-(void)viewDidAppear:(BOOL)animated
{
	
	[super viewDidAppear:animated];
	//[[HM_AppDelegate sharedAppDelegate] showTitle];
	_hiddenTable.hidden = YES;
	[_hiddenTable reloadData];
	[_tableSearchPage reloadData];
}


@end

@implementation searchCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleNone;
		
	}
	return self;
	
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	
	self.textLabel.backgroundColor = [UIColor clearColor];
	if (self.textLabel.text == nil) {
		self.backgroundColor = [UIColor clearColor];
	}
	else if (self.textLabel.text != nil) {
		self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];		//0.7
		self.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
	}
}

@end

