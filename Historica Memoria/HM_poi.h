//
//  HM_poi.h
//  Historica Memoria
//
//  Created by Jan Baros on 12/18/12.
//
//

#import <Foundation/Foundation.h>
#import "HM_EventData.h"
#import "Argol.h"


@interface HM_poi : NSObject

@property (nonatomic,retain) HM_EventData *event;
@property (nonatomic, retain) ArgolTarget *target;
@end
