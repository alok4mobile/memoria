//
//  HM_FullPagePOIViewController.m
//  Historica Memoria
//
//  Created by Jan Baros on 11/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HM_FullPagePOIViewController.h"

@interface HM_FullPagePOIViewController ()

@end

@implementation HM_FullPagePOIViewController

- (id)initWithImageA:(NSString *)imageA ImageB:(NSString *)imageB
{
    self = [super init];
    if (self) {
		
		_imageA = [UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",imageA]];
		_imageB = [UIImage imageNamed:[NSString stringWithFormat:@"%@_m.jpg",imageB]];
		NSLog(@"image A %@",[NSString stringWithFormat:@"%@_m.jpg",imageA]);
		NSLog(@"image B %@",[NSString stringWithFormat:@"%@_m.jpg",imageB]);
		
		self.wantsFullScreenLayout = YES;
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.view.backgroundColor = [UIColor clearColor];
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	CGRect surfaceFrame =	[[UIScreen mainScreen] applicationFrame];
	
	
	surfaceScroll = [[UIScrollView alloc] initWithFrame:surfaceFrame];
	surfaceScroll.backgroundColor = [UIColor clearColor];
	UIImage *imageA = _imageA;
	UIImage *imageB = _imageB;
	
	_pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(10, (self.view.frame.size.height/2) - 10, 20, 20)];
	_pageControl.transform = CGAffineTransformMakeRotation(3.14/2);	
	_pageControl.numberOfPages = 2;
	_pageControl.backgroundColor = [UIColor clearColor];
	
	if (imageB != nil && imageA != nil) {
		
		CGRect frame;
		frame.origin.x = 0;
		frame.origin.y = 0;
		frame.size = surfaceScroll.frame.size;
		
		arImageA = [[UIImageView alloc] initWithImage:imageA];
		if (imageA.size.height < imageA.size.width) {
			arImageA.transform = CGAffineTransformMakeRotation(3.14/2);	
		}
		
		arImageA.frame = frame;
		[surfaceScroll addSubview:arImageA];
		frame.origin.y = arImageA.frame.size.height;
		
		arImageB = [[UIImageView alloc] initWithImage:imageB];
		if (imageB.size.height < imageB.size.width) {
			arImageB.transform = CGAffineTransformMakeRotation(3.14/2);
		}
		arImageB.frame = frame;
		[surfaceScroll addSubview:arImageB];
	 	surfaceScroll.contentSize = CGSizeMake(surfaceScroll.frame.size.width, surfaceScroll.frame.size.height*2);
		
	}	
	
	else if (imageB == nil || imageA == nil ) {
		CGRect frame;
		frame.origin.x = 0;
		frame.origin.y = 0;
		frame.size = surfaceScroll.frame.size;
		arImageA = [[UIImageView alloc] initWithImage:(imageA!=nil)?imageA:imageB];
		if (arImageA.image.size.height < arImageA.image.size.width) {
			arImageA.transform = CGAffineTransformMakeRotation(3.14/2);	
		}

		arImageA.frame = frame;
		[surfaceScroll addSubview:arImageA];
		surfaceScroll.contentSize = CGSizeMake(surfaceScroll.frame.size.width, surfaceScroll.frame.size.height);
	}
	
	
	CGFloat closeButtonXoffset = surfaceFrame.size.width - 40;
	CGFloat closeButtonYoffset = surfaceFrame.size.height - 40;
	
	UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(closeButtonXoffset, closeButtonYoffset, 35, 35)];
	[closeButton setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
	[closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
	
	surfaceScroll.showsVerticalScrollIndicator = NO;
	surfaceScroll.pagingEnabled = YES;
	surfaceScroll.delegate = self;
	
	[self.view addSubview:surfaceScroll];
	if (imageB != nil && imageA != nil)[self.view addSubview:_pageControl];
	[self.view addSubview:closeButton];	
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	CGFloat pageHeight = self.view.frame.size.height;
	
	int page = floor((surfaceScroll.contentOffset.y - pageHeight /2) /pageHeight) +1;
	NSLog(@"page = %i",page);
	_pageControl.currentPage = page;
}


-(void) close {
	[self dismissModalViewControllerAnimated:YES];
}

@end
