//
//  HM_LandingViewController.h
//  Historica Memoria
//
//  Created by Jan Baros on 8/14/13.
//
//

#import <UIKit/UIKit.h>
#import "HM_EventData.h"
#import "HM_DetailViewController.h"
#import "HM_AppDelegate.h"
#import "HM_poi.h"

@class topButton;

@interface HM_LandingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UINavigationBarDelegate> {
	UITableView *_eventsTable;
	NSString *_mainTitle;
	NSMutableArray *_tableData;
	NSMutableArray *_topButtons;
	NSMutableArray *_tableViews;
	UIScrollView *_pageScroll;
	//UIScrollView *topScroll;
	UIView *arView;
	int views;
	int returnIndex;
	UIBarButtonItem *myBarButtonItem;
	BOOL _pageScrollisHidden;
	int resetToPage;
}

-(void)arPressed;

@property (strong, nonatomic) UIScrollView *topscroll;
@property (nonatomic,retain) UILabel *poiDistanceLabel;
@property (nonatomic, retain) ArgolTarget *firstClosestTarget;
@property (nonatomic, retain) ArgolTarget *secondClosestTarget;
@property (nonatomic, retain) ArgolTarget *thirdClosestTarget;

@end
